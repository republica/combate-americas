<?php
/*
Template Name: ABOUT
*/
?>
<?php get_header(); ?>

    <main class="about-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="main-heading">
                <div class="background">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="wrapper">
                    <h1 class="mach">Combate<br>Americas</h1>
                    <h4><?php echo __('THE 1ST FIRST U.S. HISPANIC MMA SPORTS FRANCHISE IN HISTORY'); ?></h4>
                </div>
            </div>
            <div class="sub-content">
                <div class="wrapper">
                   <?php the_content(); ?>
                </div> <!-- .wrapper -->
            </div>
        </section>
    </main>

<?php get_footer(); ?>