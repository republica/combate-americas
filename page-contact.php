<?php
/*
Template Name: CONTACT
*/
?>
<?php get_header(); ?>
    <main class="contact-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="main-heading">
                <div class="background">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="wrapper">
                    <h1 class="mach"><?php the_title(); ?></h1>
                </div>
            </div>
            <div class="sub-content">
                <div class="wrapper">
                    <div class="contact-section">
                        <div class="form">
                           <?php the_content(); ?>
                        </div>
                    </div>
                </div> <!-- .wrapper -->
            </div>
        </section>
    </main>
<?php get_footer(); ?>