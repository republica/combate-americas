<?php
/*
Template Name: Sizzle Reel
*/
if ( $_GET['access'] != 'st0p')  { wp_redirect( home_url() ); exit; }
?>

<?php get_header(); ?>

<main class="news-page">

    <section class="hero">
        <div class="background">
            <?php the_post_thumbnail(); ?>
        </div>
        <div class="heading">'
            <div class="wrap">
                <h1 class="mach">
                    <?php the_title(); ?>
                </h1>
            </div>
        </div>
    </section>
    <?php
    if ( $_GET['access'] == 'st0p') {
        // the_content();
        // Code to fetch and print CFs, such as:
        $key_1_value_1 = get_post_meta( $post->ID, 'key_1', true );
            echo $key_1_value_1;
    
    $videosID = get_field('reel_videos');
    $args = array(
        'post_type'     => 'video-reel',
        'posts_per_page' => -1,
        'posts__in' => $videosID,
        'orderby'       => 'date',
        'order'         => 'DESC',
    );
    query_posts($args);
    if (have_posts()) : ?>
    <section class="content">           
        <div class="sub-content">
            <div class="news-wrapper content-slider-wrapper">
                <ul class="news content-slider">
                    <?php while (have_posts()) : the_post(); ?>
                        <?php include( locate_template( 'parts/video-card.php', false, false ) ); ?>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </section>
<?php endif; wp_reset_query(); ?>
<?php } ?>
</main>

<?php get_footer(); ?>