<?php
/*
Template Name: NEWS
*/
?>
<?php get_header(); ?>

<main class="news-page">
    <?php
    $args = array(
        'post_type'     => 'post',
        'posts_per_page' => -1,
        'orderby'       => 'date',
        'order'         => 'DESC',
    );
    query_posts($args); 
    $i = 0;
    if (have_posts()) :
        while (have_posts()) : the_post();  $i++; 

            if ($i == 1)   { ?>

            <section class="hero">
                <div class="background">
                    <?php the_post_thumbnail() ?>
                </div>
                <div class="heading">
                    <div class="wrap">
                        <h1 class="mach"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
                    </div>
                    <div class="wrap">
                        <p class="video-text"><?php the_excerpt(); ?></p>
                    </div>
                </div>
            </section>

            <?php }

            if ($i == 2)   { ?>
            <section class="content">           
                <div class="sub-content">
                    <div class="news-wrapper content-slider-wrapper">
                        <ul class="news content-slider">
                            <?php }
                        if ($i >= 2)   {
                                include( locate_template( 'parts/article-card.php', false, false ) );
                            } ?>

                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </section>
        <?php endif; ?>
    </main>

    <?php get_footer(); ?>