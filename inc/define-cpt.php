<?php 

/* CUSTOM POST TYPES AND CUSTOM TAXONOMIES DEFINITION */


/*--------------------------------------------------------------------------------------------------*/
/* Custom Post Types
----------------------------------------------------------------------------------------------------*/

/* Video */

add_action( 'init', 'register_cpt_video' );

function register_cpt_video() {

    $labels = array( 
        'name'               => _x( 'Video', 'video' ),
        'singular_name'      => _x( 'Video', 'video' ),
        'add_new'            => _x( 'Add new', 'video' ),
        'add_new_item'       => _x( 'Create new video', 'video' ),
        'edit_item'          => _x( 'Edit', 'video' ),
        'new_item'           => _x( 'New', 'video' ),
        'view_item'          => _x( 'View', 'video' ),
        'search_items'       => _x( 'Search', 'video' ),
        'not_found'          => _x( 'No found', 'video' ),
        'not_found_in_trash' => _x( 'No in the trash', 'video' ),
        'parent_item_colon'  => _x( 'Video Superior:', 'video' ),
        'menu_name'          => _x( 'Videos', 'video' ),
        );

    $args = array( 
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => "Video",
        'supports'            => array( 'title','excerpt','editor','thumbnail'),
        'taxonomies'          => array('post_tag'),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'menu_icon'           => 'dashicons-video-alt',
        'capability_type'     => 'post'
        );

    register_post_type( 'video', $args );
}

/* Video */

add_action( 'init', 'register_cpt_video_reel' );

function register_cpt_video_reel() {

    $labels = array( 
        'name'               => _x( 'Video - Reel', 'video-reel' ),
        'singular_name'      => _x( 'Video', 'video-reel' ),
        'add_new'            => _x( 'Add new', 'video-reel' ),
        'add_new_item'       => _x( 'Create new video', 'video-reel' ),
        'edit_item'          => _x( 'Edit', 'video-reel' ),
        'new_item'           => _x( 'New', 'video-reel' ),
        'view_item'          => _x( 'View', 'video-reel' ),
        'search_items'       => _x( 'Search', 'video-reel' ),
        'not_found'          => _x( 'No found', 'video-reel' ),
        'not_found_in_trash' => _x( 'No in the trash', 'video-reel' ),
        'parent_item_colon'  => _x( 'Video Superior:', 'video-reel' ),
        'menu_name'          => _x( 'Videos - Reel', 'video-reel' ),
        );

    $args = array( 
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => "Video",
        'supports'            => array( 'title','excerpt','editor','thumbnail'),
        'taxonomies'          => array('post_tag'),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'menu_icon'           => 'dashicons-video-alt',
        'capability_type'     => 'post'
        );

    register_post_type( 'video-reel', $args );
}

/* Fighter */

add_action( 'init', 'register_cpt_fighter' );

function register_cpt_fighter() {

    $labels = array( 
        'name'               => _x( 'Fighter', 'fighter' ),
        'singular_name'      => _x( 'Fighter', 'fighter' ),
        'add_new'            => _x( 'Add new', 'fighter' ),
        'add_new_item'       => _x( 'Create new fighter', 'fighter' ),
        'edit_item'          => _x( 'Edit', 'fighter' ),
        'new_item'           => _x( 'New', 'fighter' ),
        'view_item'          => _x( 'View', 'fighter' ),
        'search_items'       => _x( 'Search', 'fighter' ),
        'not_found'          => _x( 'No found', 'fighter' ),
        'not_found_in_trash' => _x( 'No in the trash', 'fighter' ),
        'parent_item_colon'  => _x( 'Fighter Superior:', 'fighter' ),
        'menu_name'          => _x( 'Fighters', 'fighter' ),
        );

    $args = array( 
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => "Fighter",
        'supports'            => array( 'title','excerpt','editor','thumbnail', 'page-attributes'),
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'menu_icon'           => 'dashicons-groups',
        'capability_type'     => 'post'
        );

    register_post_type( 'fighter', $args );
}


/* Fight */

add_action( 'init', 'register_cpt_fight' );

function register_cpt_fight() {

    $labels = array( 
        'name'               => _x( 'Fight', 'fight' ),
        'singular_name'      => _x( 'Fight', 'fight' ),
        'add_new'            => _x( 'Add new', 'fight' ),
        'add_new_item'       => _x( 'Create new fight', 'fight' ),
        'edit_item'          => _x( 'Edit', 'fight' ),
        'new_item'           => _x( 'New', 'fight' ),
        'view_item'          => _x( 'View', 'fight' ),
        'search_items'       => _x( 'Search', 'fight' ),
        'not_found'          => _x( 'No found', 'fight' ),
        'not_found_in_trash' => _x( 'No in the trash', 'fight' ),
        'parent_item_colon'  => _x( 'Fight Superior:', 'fight' ),
        'menu_name'          => _x( 'Fights', 'fight' ),
        );

    $args = array( 
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => "Fight",
        'supports'            => array( 'title','excerpt','editor','thumbnail'),
        'taxonomies'          => array('post_tag'),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'menu_icon'           => 'dashicons-star-half',
        'capability_type'     => 'post'
        );

    register_post_type( 'fight', $args );
}

/* Event */

add_action( 'init', 'register_cpt_event' );

function register_cpt_event() {

    $labels = array( 
        'name'               => _x( 'Event', 'event' ),
        'singular_name'      => _x( 'Event', 'event' ),
        'add_new'            => _x( 'Add new', 'event' ),
        'add_new_item'       => _x( 'Create new event', 'event' ),
        'edit_item'          => _x( 'Edit', 'event' ),
        'new_item'           => _x( 'New', 'event' ),
        'view_item'          => _x( 'View', 'event' ),
        'search_items'       => _x( 'Search', 'event' ),
        'not_found'          => _x( 'No found', 'event' ),
        'not_found_in_trash' => _x( 'No in the trash', 'event' ),
        'parent_item_colon'  => _x( 'Event Superior:', 'event' ),
        'menu_name'          => _x( 'Events', 'event' ),
        );

    $args = array( 
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => "Event",
        'supports'            => array( 'title','excerpt','editor','thumbnail'),
        'taxonomies'          => array('post_tag'),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'menu_icon'           => 'dashicons-tickets-alt',
        'capability_type'     => 'post'
        );

    register_post_type( 'event', $args );
}

/* Bracket */

add_action( 'init', 'register_cpt_bracket' );

function register_cpt_bracket() {

    $labels = array( 
        'name'               => _x( 'Bracket', 'bracket' ),
        'singular_name'      => _x( 'Bracket', 'bracket' ),
        'add_new'            => _x( 'Add new', 'bracket' ),
        'add_new_item'       => _x( 'Create new bracket', 'bracket' ),
        'edit_item'          => _x( 'Edit', 'bracket' ),
        'new_item'           => _x( 'New', 'bracket' ),
        'view_item'          => _x( 'View', 'bracket' ),
        'search_items'       => _x( 'Search', 'bracket' ),
        'not_found'          => _x( 'No found', 'bracket' ),
        'not_found_in_trash' => _x( 'No in the trash', 'bracket' ),
        'parent_item_colon'  => _x( 'Bracket Superior:', 'bracket' ),
        'menu_name'          => _x( 'Brackets', 'bracket' ),
        );

    $args = array( 
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => "bracket",
        'supports'            => array( 'title','excerpt','editor','thumbnail'),
        'taxonomies'          => array('post_tag'),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'menu_icon'           => 'dashicons-networking',
        'capability_type'     => 'post'
        );

    register_post_type( 'bracket', $args );
}

/*--------------------------------------------------------------------------------------------------*/
/* Custom Taxonomies
----------------------------------------------------------------------------------------------------*/

/* Sex */

add_action( 'init', 'register_taxonomy_sex' );

function register_taxonomy_sex() {

    $labels = array( 
        'name'                       => _x( 'Sex', 'sex' ),
        'singular_name'              => _x( 'Sex', 'sex' ),
        'search_items'               => _x( 'Searc', 'sex' ),
        'popular_items'              => _x( 'More populars', 'sex' ),
        'all_items'                  => _x( 'All', 'sex' ),
        'parent_item'                => _x( 'Parent', 'sex' ),
        'parent_item_colon'          => _x( 'Parent', 'sex' ),
        'edit_item'                  => _x( 'Edit', 'sex' ),
        'update_item'                => _x( 'Update', 'sex' ),
        'add_new_item'               => _x( 'Create new', 'sex' ),
        'new_item_name'              => _x( 'New', 'sex' ),
        'separate_items_with_commas' => _x( 'Separate with commas(,)', 'sex' ),
        'add_or_remove_items'        => _x( 'Add or remove', 'sex' ),
        'choose_from_most_used'      => _x( 'Choose from the most used', 'sex' ),
        'menu_name'                  => _x( 'Sex', 'sex' ),
        );

    $args = array( 
        'labels'            => $labels,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_ui'           => true,
        'show_tagcloud'     => true,
        'hierarchical'      => true,
        'rewrite'           => true,
        'query_var'         => true
        );

    register_taxonomy( 'sex', array('fighter'), $args );
}

/* Weight Class */

add_action( 'init', 'register_taxonomy_weight_class' );

function register_taxonomy_weight_class() {

    $labels = array( 
        'name'                       => _x( 'Weight class', 'weight-class' ),
        'singular_name'              => _x( 'Weight class', 'weight-class' ),
        'search_items'               => _x( 'Search', 'weight-class' ),
        'popular_items'              => _x( 'More populars', 'weight-class' ),
        'all_items'                  => _x( 'All', 'weight-class' ),
        'parent_item'                => _x( 'Parent', 'weight-class' ),
        'parent_item_colon'          => _x( 'Parent', 'weight-class' ),
        'edit_item'                  => _x( 'Edit', 'weight-class' ),
        'update_item'                => _x( 'Update', 'weight-class' ),
        'add_new_item'               => _x( 'Create new', 'weight-class' ),
        'new_item_name'              => _x( 'New', 'weight-class' ),
        'separate_items_with_commas' => _x( 'Separate with commas(,)', 'weight-class' ),
        'add_or_remove_items'        => _x( 'Add or remove', 'weight-class' ),
        'choose_from_most_used'      => _x( 'Choose from the most used', 'weight-class' ),
        'menu_name'                  => _x( 'Weight class', 'weight-class' ),
        );

    $args = array( 
        'labels'            => $labels,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_ui'           => true,
        'show_tagcloud'     => true,
        'hierarchical'      => true,
        'rewrite'           => true,
        'query_var'         => true
        );

    register_taxonomy( 'weight-class', array('fighter'), $args );
}

/* Fighting Styles */

add_action( 'init', 'register_taxonomy_fighting_style' );

function register_taxonomy_fighting_style() {

    $labels = array( 
        'name'                       => _x( 'Fighting style', 'fighting-style' ),
        'singular_name'              => _x( 'Fighting style', 'fighting-style' ),
        'search_items'               => _x( 'Search', 'fighting-style' ),
        'popular_items'              => _x( 'More populars', 'fighting-style' ),
        'all_items'                  => _x( 'All', 'fighting-style' ),
        'parent_item'                => _x( 'Parent', 'fighting-style' ),
        'parent_item_colon'          => _x( 'Parent', 'fighting-style' ),
        'edit_item'                  => _x( 'Edit', 'fighting-style' ),
        'update_item'                => _x( 'Update', 'fighting-style' ),
        'add_new_item'               => _x( 'Create new', 'fighting-style' ),
        'new_item_name'              => _x( 'New', 'fighting-style' ),
        'separate_items_with_commas' => _x( 'Separate with commas(,)', 'fighting-style' ),
        'add_or_remove_items'        => _x( 'Add or remove', 'fighting-style' ),
        'choose_from_most_used'      => _x( 'Choose from the most used', 'fighting-style' ),
        'menu_name'                  => _x( 'Fighting style', 'fighting-style' ),
        );

    $args = array( 
        'labels'            => $labels,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_ui'           => true,
        'show_tagcloud'     => true,
        'hierarchical'      => true,
        'rewrite'           => true,
        'query_var'         => true
        );

    register_taxonomy( 'fighting-style', array('fighter'), $args );
}

/* Country */

add_action( 'init', 'register_taxonomy_country' );

function register_taxonomy_country() {

    $labels = array( 
        'name'                       => _x( 'Country', 'country' ),
        'singular_name'              => _x( 'Country', 'country' ),
        'search_items'               => _x( 'Search', 'country' ),
        'popular_items'              => _x( 'More populars', 'country' ),
        'all_items'                  => _x( 'All', 'country' ),
        'parent_item'                => _x( 'Parent', 'country' ),
        'parent_item_colon'          => _x( 'Parent', 'country' ),
        'edit_item'                  => _x( 'Edit', 'country' ),
        'update_item'                => _x( 'Update', 'country' ),
        'add_new_item'               => _x( 'Create new', 'country' ),
        'new_item_name'              => _x( 'New', 'country' ),
        'separate_items_with_commas' => _x( 'Separate with commas(,)', 'country' ),
        'add_or_remove_items'        => _x( 'Add or remove', 'country' ),
        'choose_from_most_used'      => _x( 'Choose from the most used', 'country' ),
        'menu_name'                  => _x( 'Country', 'country' ),
        );

    $args = array( 
        'labels'            => $labels,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_ui'           => true,
        'show_tagcloud'     => true,
        'hierarchical'      => true,
        'rewrite'           => true,
        'query_var'         => true
        );

    register_taxonomy( 'country', array('fighter'), $args );
}


function bidirectional_acf_update_value( $value, $post_id, $field  ) {
    
    // vars
    $field_name = $field['name'];
    $field_key = $field['key'];
    $global_name = 'is_updating_' . $field_name;    
    
    // bail early if this filter was triggered from the update_field() function called within the loop below
    // - this prevents an inifinte loop
    if( !empty($GLOBALS[ $global_name ]) ) return $value;
    
    
    // set global variable to avoid inifite loop
    // - could also remove_filter() then add_filter() again, but this is simpler
    $GLOBALS[ $global_name ] = 1;    
    
    // loop over selected posts and add this $post_id
    if( is_array($value) ) {
        foreach( $value as $post_id2 ) {
            // load existing related posts
            $value2 = get_field($field_name, $post_id2, false);
            // allow for selected posts to not contain a value
            if( empty($value2) ) {
                $value2 = array();
            }
            
            // bail early if the current $post_id is already found in selected post's $value2
            if( in_array($post_id, $value2) ) continue;
            
            // append the current $post_id to the selected post's 'related_posts' value
            $value2[] = $post_id;
            
            // update the selected post's value (use field's key for performance)
            update_field($field_key, $value2, $post_id2);
            
        }
    
    }   
    
    // find posts which have been removed
    $old_value = get_field($field_name, $post_id, false);
    
    if( is_array($old_value) ) {
        
        foreach( $old_value as $post_id2 ) {
            // bail early if this value has not been removed
            if( is_array($value) && in_array($post_id2, $value) ) continue;                       
            // load existing related posts
            $value2 = get_field($field_name, $post_id2, false);          
            // bail early if no value
            if( empty($value2) ) continue;
            // find the position of $post_id within $value2 so we can remove it
            $pos = array_search($post_id, $value2);
            // remove
            unset( $value2[ $pos] );
            // update the un-selected post's value (use field's key for performance)
            update_field($field_key, $value2, $post_id2);
        }
    }
    
    // reset global varibale to allow this filter to function as per normal
    $GLOBALS[ $global_name ] = 0;
    
    // return
    return $value;
    
}


// NEWS
add_filter('acf/update_value/name=news_fighters', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT
add_filter('acf/update_value/name=news_fights', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT
add_filter('acf/update_value/name=news_events', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT
add_filter('acf/update_value/name=related_news', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT
add_filter('acf/update_value/name=related_news_videos', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT

// VIDEOS
add_filter('acf/update_value/name=videos_fighters', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT
add_filter('acf/update_value/name=videos_fights', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT
add_filter('acf/update_value/name=videos_events', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT
add_filter('acf/update_value/name=related_videos', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT
add_filter('acf/update_value/name=related_news_videos', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT

// EVENTS / FIGHTS
add_filter('acf/update_value/name=event_fights', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT

// FIGHTS/FIGHTERS
add_filter('acf/update_value/name=fight_fighters', 'bidirectional_acf_update_value', 10, 3); // Fights of the Event > EVENT, FIGHT


// add_action('admin_head', 'my_custom_styles');
function my_custom_styles() {
  echo '<style>
    .menu-icon-page {  /*background-color: #cccccc !important;*/ //border-bottom: 3px solid black;  }
    .menu-icon-post { background-color: brown !important;  border-bottom: 1px solid rgba(255, 255, 255, 0.3);  }
    .menu-icon-post ul { background-color: brown !important; }
    .menu-icon-fighter {  background-color: brown !important; //border-bottom: 3px solid black; }
    .menu-icon-fighter {  background-color: brown !important; }
    .menu-icon-video {  background-color: brown !important; //border-bottom: 3px solid black; }
    .menu-icon-video {  background-color: brown !important; }
    .menu-icon-fight { background-color: brown !important; //border-bottom: 3px solid black;  }
    .menu-icon-fight { background-color: brown !important;  }
    .menu-icon-event { background-color: brown !important;  border-bottom: 1px solid rgba(255, 255, 255, 0.3);  }
    .menu-icon-event { background-color: brown !important;  }
    .acf-fields > .acf-field {
        z-index: 0;
    }
  </style>';
}



?>