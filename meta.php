    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_DIRECTORY; ?>/app.css?v=1.8">
    <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_DIRECTORY; ?>/style.css?v=2.15">
    <title><?php wp_title(); ?></title>

    <link rel="icon" href="<?php echo TEMPLATE_DIRECTORY; ?>/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo TEMPLATE_DIRECTORY; ?>/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo TEMPLATE_DIRECTORY; ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo TEMPLATE_DIRECTORY; ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo TEMPLATE_DIRECTORY; ?>/favicon/manifest.json">
    <link rel="mask-icon" href="<?php echo TEMPLATE_DIRECTORY; ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <meta name="google-site-verification" content="QznZ-_SzdYjS9F0-5B1dZ8zDQ3GQcUholahCCCKf8So" />

    <?php wp_head(); ?>