<?php
/*
Template Name: RANKINGS
*/
?>
<?php get_header(); ?>

<main class="ranking-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
    -->
    <section class="content">
        <div class="main-heading">
            <div class="background">
                <?php the_post_thumbnail(); ?>
            </div>
            <div class="wrapper">
                <h1 class="mach"><?php echo __('Fighter Rankings'); ?></h1>
            </div>
        </div>
        <div class="sub-content">
            <div class="wrapper">

                <div class="tabs-scroll">
                    <ul class="tabs">
                        <?php 
                        $terms = get_terms( array(
                            'taxonomy' => 'weight-class',
                            'hide_empty' => true,
                        ) );        
                        $i = 0;
                        foreach ($terms as $term) { $i++; ?>
                        <div class="title">
                            <li class="tab-link <?php if ($i == 1) echo "current";?>" data-tab="tab-<?php echo $i; ?>"><?php echo $term->name; ?></li>
                        </div>
                        <?php } ?>
                    </ul>
                </div>

                <?php $i = 0; foreach ($terms as $term) { $i++; ?>

                <div id="tab-<?php echo $i; ?>" class="tab-content <?php if ($i == 1) echo "current";?>">
                    <table>
                        <tbody>
                            <tr>
                                <th><?php echo __('Country'); ?></th>
                                <th><?php echo __('Rank'); ?></th>
                                <th><?php echo __('Fighter'); ?></th>
                                <th><?php echo __('W'); ?></th>
                                <th><?php echo __('L'); ?></th>
                                <th><?php echo __('D'); ?></th>
                            </tr>

                            <?php

                            $args = array(
                                'post_type'     => 'fighter',
                                'posts_per_page' => -1,
                                'orderby'   => 'meta_value_num',
                                'meta_key'  => 'fighter_ranking',
                                'order'  => 'ASC',
                                'tax_query' => array(
                                    'relation' => 'AND', 
                                    array(
                                        'taxonomy' => 'weight-class',
                                        'field' => 'slug',
                                        'terms' => array($term->slug),
                                    ),
                                ),
                            );
                            query_posts($args); 
                            while (have_posts()) : the_post(); ?>
                                <tr>
                                    <?php $country = wp_get_post_terms($post->ID, 'country', array("fields" => "all"));?>
                                    <td><img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>"></td>
                                    <td><?php echo get_field('fighter_ranking'); ?></td>
                                    <td><a href=" <?php the_permalink(); ?> " title="<?php the_title(); ?>"><?php the_title(); ?></a></td>
                                    <?php $proRecord = explode('-', str_replace(' ', '', get_field('fighter_pro_record'))); ?>
                                    <td><?php echo $proRecord[0]; ?></td>
                                    <td><?php echo $proRecord[1]; ?></td>
                                    <td><?php echo $proRecord[2]; ?></td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>
                </div> <!-- #tab-1 -->

                <?php } ?>

            </div> <!-- .wrapper -->
        </div>
    </section>
</main>

<?php get_footer(); ?>