<div class="watch-btn">
    <div class="info">
        <a class="selected-link" href="http://www.telemundo.com/" target="_blank">
            <div class="logo">
                <img class="tele-logo" src="../img/logo-tele.svg" alt="">
                <img class="tvaz-logo" src="../img/logo-tv-azteca.png" alt="">
            </div>
            <div class="text">
                <h4>Watch live on</h4>
                <h3>telemundo</h3>
            </div>
        </a>
    </div>
    <div class="menu">
        <img src="../img/icon-watch-menu.svg" alt="">
    </div>
    <ul class="subnav">
        <li>
            <div class="logo">
                <img src="../img/icon-telemundo.png" alt="">
            </div>
            <div class="channel-num">441</div>
            <div class="channel-name">Telemundo</div>
            <div class="play">
                <a class="btn round small border yellow" href="#">
                    <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                </a>
            </div>
        </li>
        <li>
            <div class="logo">
                <img src="../img/icon-tvazteca.png" alt="">
            </div>
            <div class="channel-num">441</div>
            <div class="channel-name">TV Azteca</div>
            <div class="play">
                <a class="btn round small border yellow" href="#">
                    <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                </a>
            </div>
        </li>
    </ul>
</div>