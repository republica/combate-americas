<?php require 'html-header.php' ?>

    <main class="home-page-pre-fight home-page">
        <!-- 
        ******************************
        *
        *   Hero
        *
        ******************************
        -->
        <section class="hero">
            <div class="background">
                <img src="../img/hero-pre-fight.jpg" alt="">
            </div>
            <div class="heading">
                <div class="wrap">
                    <div class="copa-logo">
                        <img src="../img/logo-copa-combate.png" alt="">
                    </div>
                </div>
                <div class="wrap">
                    <h3 class="mach">Oct 30</h3>
                    <h3 class="robo">9PM - 11PM</h3>
                    <div class="watch-live">
                        <?php require 'html-watch-btn.php' ?>
                    </div>
                </div>
                <div class="wrap">
                    <h3>Mexico City - Name of Venue</h3>
                </div>
                <div class="wrap">
                    <a href="#" class="btn yellow long">
                        about event
                    </a>
                </div>
            </div>
        </section>
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            
            <div class="sub-content brackets">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">TOURNAMENT BRACKET</h2>
                    <h3 class="live">Live</h3>
                </div>

                <div class="round-container">
                    <ul class="round-nav">
                        <li class="round-link current" data-round="round-1">Round 1</li>
                        <li class="round-link" data-round="round-2">Round 2</li>
                        <li class="round-link" data-round="round-3">Round 3</li>
                        <li class="round-link" data-round="round-4">Winner</li>
                    </ul>
                </div>
                <div class="round-bracket-container">
                    <div class="rounds">
                        <!-- 
                        *****************
                        *    ROUND 1 
                        ***************** 
                        -->
                        <div id="round-1" class="round round-content current">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                            <!-- 
                            *****************
                            *    FIGHT 2 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                            <!-- 
                            *****************
                            *    FIGHT 3 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                            <!-- 
                            *****************
                            *    FIGHT 4 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                        </div> <!-- .round -->

                        <!-- 
                        *****************
                        *    ROUND 2
                        ***************** 
                        -->
                        <div id="round-2" class="round round-content">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                            <!-- 
                            *****************
                            *    FIGHT 2 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                        </div> <!-- .round -->

                        <!-- 
                        *****************
                        *    ROUND 3
                        ***************** 
                        -->
                        <div id="round-3" class="round round-content">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->
                            
                        </div> <!-- .round -->

                        <!-- 
                        *****************
                        *    ROUND 4
                        ***************** 
                        -->
                        <div id="round-4" class="round round-content">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight">
                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="score">
                                                12
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->
                            
                        </div> <!-- .round -->

                    </div> <!-- .rounds -->
                </div> <!-- .round-bracket-container -->
                
            </div> <!-- .sub-content -->

        </section>
    </main>

<?php require 'html-footer.php' ?>