<div class="popup-container">
    <div class="overlay"></div>
    <div class="main-content">
        
        <div class="close-btn">
            <img class="svg" src="../img/icon-close.svg" alt="">
        </div>

        <div class="container contact-form">
            <h2>Congratulations!</h2>
            <p>You’ve completed your bracket. Now, enter your name and email below to be entered to win a trip to our next fight in San Antonio.</p>
            <input type="text" placeholder="Full Name">
            <input type="email" name="" id="" placeholder="Email">
            <div class="check-wrap">
                <label for="check">
                    <input type="checkbox" name="check" id="check">
                </label>
                <p>By checking you verify that you are at least 18 years old.</p>
            </div>
            <button class="btn long yellow">Submit</button>
        </div>

        <div class="container hidden contact-form-sent">
            <h2>Thank you for entering the Copa Combate Bracket Challenge</h2>
            <p>Tune in to Telemundo on November 11th to see how your bracket holds up and watch the Quest for the Unbeatable Nation unfold.</p>
            <p>To increase your chances of winning the grand prize, challenge your friends on social.</p>
            <button class="btn long yellow"></button>
        </div>
    </div>
</div>