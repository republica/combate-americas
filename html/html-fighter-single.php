<?php require 'html-header.php' ?>

    <main class="fighter-single-page">

        <!-- <div class="fighter-nav">
            <a href=""><img class="left" src="../img/icon-arrow-rounded.svg" alt="">Prev</a>
            <a href="">All Fighters</a>
            <a href="">Next<img class="right" src="../img/icon-arrow-rounded.svg" alt=""></a>
        </div> -->


        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="main-heading single-fighter">
                <ul class="fighter-intro-container">
                    <li>
                        <div class="fighter-intro">
                            <div class="social-media">
                                <ul class="icons">
                                    <li><a href="#"><img src="../img/icons-fighter/fb.svg" alt=""></a></li>
                                    <li><a href="#"><img src="../img/icons-fighter/tw.svg" alt=""></a></li>
                                    <li><a href="#"><img src="../img/icons-fighter/ig.svg" alt=""></a></li>
                                    <li><a href="#"><img src="../img/icons-fighter/yt.svg" alt=""></a></li>
                                </ul>
                            </div>
                            <div class="wrapper">
                                <div class="background">
                                    <img src="../img/fighter-jose.png" alt="">
                                </div>
                                <div class="info">
                                    <div class="info-top">
                                        <div class="name">
                                            <h4 class="robo">Jose</h4>
                                            <h1 class="mach">Estrada</h1>
                                            <span>"Froggy"</span>
                                        </div>
                                        <div class="weight-class">
                                            <p>Feather Weight</p>
                                            <img src="../img/flag.png" alt="">
                                        </div>
                                    </div>
                                    <div class="info-bottom-alt">
                                        <div class="wins">
                                            <h3 class="mach">04</h3>
                                            <p>Wins</p>
                                        </div>
                                        <div class="losses">
                                            <h3 class="mach">01</h3>
                                            <p>Losses</p>
                                        </div>
                                        <button class="btn round yellow">
                                            <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                        </button>
                                    </div>
                                </div>
                            </div> <!-- .wrapper -->
                        </div> <!-- .fighter-intro -->
                        <div class="fighter-stats">
                            <div class="wrapper">
                                <ul class="tabs">
                                    <li class="tab-link current" data-tab="tab-1">Details</li>
                                    <li class="tab-link" data-tab="tab-2">Record</li>
                                    <li class="tab-link" data-tab="tab-3">Bio</li>
                                </ul>

                                <div id="tab-1" class="tab-content current">
                                    <table>
                                        <tr>
                                            <td>Age</td>
                                            <td>26</td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth</td>
                                            <td>12.10.1990</td>
                                        </tr>
                                        <tr>
                                            <td>Weight Class</td>
                                            <td>Feather Weight</td>
                                        </tr>
                                        <tr>
                                            <td>Last Weigh-In</td>
                                            <td>155.2 lbs</td>
                                        </tr>
                                        <tr>
                                            <td>Height</td>
                                            <td>5'6" (168cm)</td>
                                        </tr>
                                        <tr>
                                            <td>Reach</td>
                                            <td>68.0" (173cm)</td>
                                        </tr>
                                        <tr>
                                            <td>Born</td>
                                            <td>Fillmore, California</td>
                                        </tr>
                                        <tr>
                                            <td>Fighting out of</td>
                                            <td>Ventura, California</td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="tab-2" class="tab-content">
                                    <ul>
                                        <li>Bacon ipsum dolor amet</li>
                                        <li>meatloaf boudin flank porchetta brisket</li>
                                        <li>ground round biltong sirloin pastrami</li>
                                        <li>fatback meatloaf short loin. Tail meatball swine turducken.</li>
                                        <li>Pancetta hamburger frankfurter ball tip tri-tip</li>
                                        <li>Bacon ipsum dolor amet</li>
                                        <li>meatloaf boudin flank porchetta brisket</li>
                                        <li>ground round biltong sirloin pastrami</li>
                                        <li>fatback meatloaf short loin. Tail meatball swine turducken.</li>
                                        <li>Pancetta hamburger frankfurter ball tip tri-tip</li>
                                    </ul>
                                </div>
                                <div id="tab-3" class="tab-content">
                                    Test 3
                                </div>
                            </div>
                        </div> <!-- .fighter-stats -->
                    </li>
                </ul>
            </div> <!-- .heading --> 

            <div class="sub-content pictures">

                <div class="title white">
                    <h2 class="robo">Pictures</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>
                <div class="pic-wrapper">
                    <ul class="pictures-container">
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                        <li><img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt=""></li>
                    </ul>
                </div>
            </div>
            <div class="sub-content videos">

                <div class="title white">
                    <h2 class="robo">Videos</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>

                <div class="vid-wrapper content-slider-wrapper">
                    <ul class="videos content-slider">
                        <li onclick="location.href='#'" class="card">
                            <a href="#">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </li>
                        <li onclick="location.href='#'" class="card">
                            <a href="#">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </li>
                        <li onclick="location.href='#'" class="card">
                            <a href="#">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="sub-content news">

                <div class="title white">
                    <h2 class="robo">Jose's News</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>

                <div class="news-wrapper content-slider-wrapper">
                    <ul class="news content-slider">
                        <li onclick="location.href='#'" class="card">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li onclick="location.href='#'" class="card">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li onclick="location.href='#'" class="card">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>

        </section>
    </main>

<?php require 'html-footer.php' ?>