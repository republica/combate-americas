<?php require 'html-header.php' ?>

    <main class="ranking-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="main-heading">
                <div class="background">
                    <img src="../img/hero.png" alt="">
                </div>
                <div class="wrapper">
                    <h1 class="mach">Fighter Rankings</h1>
                </div>
            </div>
            <div class="sub-content">
                <div class="wrapper">

                    <div class="tabs-scroll">
                        <ul class="tabs">
                            <li class="tab-link current" data-tab="tab-1">Pound-For-Pound</li>
                            <li class="tab-link" data-tab="tab-2">Heavyweight</li>
                            <li class="tab-link" data-tab="tab-3">Middleweight</li>
                            <li class="tab-link" data-tab="tab-4">Welterweight</li>
                            <li class="tab-link" data-tab="tab-5">Lightweight</li>
                            <li class="tab-link" data-tab="tab-6">Featherweight</li>
                        </ul>
                    </div>

                    <div id="tab-1" class="tab-content current">
                        <table>
                            <tbody>
                                <tr>
                                    <th>Country</th>
                                    <th>Rank</th>
                                    <th>Fighter</th>
                                    <!-- <th colspan="3">W/L/D</th> -->
                                    <th>W</th>
                                    <th>L</th>
                                    <th>D</th>
                                </tr>
                                <tr>
                                    <td><img src="../img/flag.png" alt=""></td>
                                    <td>00</td>
                                    <td>John 'sexi mexi' castaneda</td>
                                    <td>14</td>
                                    <td>02</td>
                                    <td>00</td>
                                </tr>
                                <tr>
                                    <td><img src="../img/flag.png" alt=""></td>
                                    <td>00</td>
                                    <td>Carlos 'Lobo' Rivera</td>
                                    <td>10</td>
                                    <td>02</td>
                                    <td>00</td>
                                </tr>
                                <tr>
                                    <td><img src="../img/flag.png" alt=""></td>
                                    <td>00</td>
                                    <td>John 'sexi mexi' castaneda</td>
                                    <td>14</td>
                                    <td>02</td>
                                    <td>00</td>
                                </tr>
                                <tr>
                                    <td><img src="../img/flag.png" alt=""></td>
                                    <td>00</td>
                                    <td>Carlos 'Lobo' Rivera</td>
                                    <td>10</td>
                                    <td>02</td>
                                    <td>00</td>
                                </tr>
                                <tr>
                                    <td><img src="../img/flag.png" alt=""></td>
                                    <td>00</td>
                                    <td>John 'sexi mexi' castaneda</td>
                                    <td>14</td>
                                    <td>02</td>
                                    <td>00</td>
                                </tr>
                                <tr>
                                    <td><img src="../img/flag.png" alt=""></td>
                                    <td>00</td>
                                    <td>Carlos 'Lobo' Rivera</td>
                                    <td>10</td>
                                    <td>02</td>
                                    <td>00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div> <!-- #tab-1 -->

                    <div id="tab-2" class="tab-content">
                    </div> <!-- #tab-2 -->

                    <div id="tab-3" class="tab-content">
                    </div> <!-- #tab-3 -->

                    <div id="tab-4" class="tab-content">
                    </div> <!-- #tab-4 -->

                    <div id="tab-5" class="tab-content">
                    </div> <!-- #tab-5 -->

                    <div id="tab-6" class="tab-content">
                    </div> <!-- #tab-6 -->

                </div> <!-- .wrapper -->
            </div>
        </section>
    </main>

<?php require 'html-footer.php' ?>