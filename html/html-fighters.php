<?php require 'html-header.php' ?>

    <main class="fighters-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="main-heading">
                <ul class="fighter-intro-container fighter-intro-slider">
                    <li>
                        <div class="fighter-intro">
                            <div class="wrapper">
                                <div class="background">
                                    <img src="../img/fighter-jose.png" alt="">
                                </div>
                                <div class="info">
                                    <div class="info-top">
                                        <div class="name">
                                            <h4 class="robo">Jose</h4>
                                            <h1 class="mach">Estrada</h1>
                                            <span>"Froggy"</span>
                                        </div>
                                        <div class="weight-class">
                                            <p>Feather Weight</p>
                                            <img src="../img/flag.png" alt="">
                                        </div>
                                    </div>
                                    <div class="info-bottom">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <a href="#" class="btn long yellow">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .fighter-intro -->
                    </li>
                    <li>
                        <div class="fighter-intro">
                            <div class="wrapper">
                                <div class="background">
                                    <img src="../img/fighter.png" alt="">
                                </div>
                                <div class="info">
                                    <div class="info-top">
                                        <div class="name">
                                            <h4 class="robo">Jose</h4>
                                            <h1 class="mach">Estrada</h1>
                                            <span>"Froggy"</span>
                                        </div>
                                        <div class="weight-class">
                                            <p>Feather Weight</p>
                                            <img src="../img/flag.png" alt="">
                                        </div>
                                    </div>
                                    <div class="info-bottom">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <button class="btn long yellow">View Profile</button>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .fighter-intro -->
                    </li>
                </ul>
            </div> <!-- .heading --> 
            <div class="sub-content">
                <!-- 
                ******************************
                *   Bantam Weight
                ******************************
                -->
                <div class="title">
                    <h1 class="mach">Bantam Weight</h1>
                </div>
                <ul class="fighters-container">
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                </ul>
                <!-- 
                ******************************
                *   Feather Weight
                ******************************
                -->
                <div class="title">
                    <h1 class="mach">Feather Weight</h1>
                </div>
                <ul class="fighters-container">
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                </ul>
                <!-- 
                ******************************
                *   Light Weight
                ******************************
                -->
                <div class="title">
                    <h1 class="mach">Light Weight</h1>
                </div>
                <ul class="fighters-container">
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                </ul>
                <!-- 
                ******************************
                *   Heavy Weight
                ******************************
                -->
                <div class="title">
                    <h1 class="mach">Heavy Weight</h1>
                </div>
                <ul class="fighters-container">
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                    <li class="fighter-card" onclick="location.href='html-fighter-single.php'">
                        <div class="image">
                            <img src="../img/fighter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="flag">
                                <img src="../img/flag.png" alt="">
                            </div>
                            <h3 class="robo">Jose</h3>
                            <h3 class="mach">Estrada</h3>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </main>

<?php require 'html-footer.php' ?>