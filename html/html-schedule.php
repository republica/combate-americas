<?php require 'html-header.php' ?>

    <main class="schedule-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="main-heading">
                <div class="background">
                    <img src="../img/hero.png" alt="">
                </div>
                <div class="wrapper">
                    <h1 class="mach">Upcoming Events</h1>
                </div>
            </div>
            <div class="sub-content">
                <div class="wrapper">
                    <div class="title-wrapper">
                        <h2 class="mach">Schedule</h2>
                    </div>
                    <ul class="tabs">
                        <li class="tab-link current" data-tab="tab-1">Upcoming Events</li>
                        <li class="tab-link" data-tab="tab-2">Past Events</li>
                    </ul>

                    <div id="tab-1" class="tab-content current">

                        <div class="date-wrapper">
                            <h2 class="robo">October 2017</h2>
                        </div>

                        <ul class="upcoming-events">
                            <li>
                                <div class="schedule-card white">
                                    <div class="date">
                                        <div class="month">OCT</div>
                                        <div class="day mach">13</div>
                                        <div class="time">6p - 10p</div>
                                    </div>
                                    <div class="info">
                                        <div class="title">Combate 17</div>
                                        <div class="location">Redlands, California</div>
                                        <div class="venue">
                                            <div class="pin">
                                                <img class="svg" src="../img/icon-pin.svg" alt="">
                                            </div>
                                            <div class="address">Splash Kingdom Amphitheater<br>Redlands, CA</div>
                                        </div>
                                        <div class="cal">
                                            <a href="">
                                                <img src="../img/icon-add.svg" alt="">
                                                <p>Add to Calendar</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="schedule-card white">
                                    <div class="date">
                                        <div class="month">OCT</div>
                                        <div class="day mach">13</div>
                                        <div class="time">6p - 10p</div>
                                    </div>
                                    <div class="info">
                                        <div class="title">Combate Americas</div>
                                        <div class="location">Azteca TV</div>
                                        <div class="venue">
                                            <div class="tv">
                                                <img class="svg" src="../img/icon-tv.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="cal">
                                            <a href="">
                                                <img src="../img/icon-add.svg" alt="">
                                                <p>Add to Calendar</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <button class="btn long yellow border">More Events</button>

                    </div> <!-- #tab-1 -->

                    <div id="tab-2" class="tab-content">

                        <div class="date-wrapper">
                            <h2 class="robo">Past Events</h2>
                        </div>

                        <ul class="past-events">
                            <li>
                                <div class="schedule-card blue">
                                    <div class="date">
                                        <div class="month">OCT</div>
                                        <div class="day mach">13</div>
                                        <div class="time">6p - 10p</div>
                                    </div>
                                    <div class="info">
                                        <div class="title">Combate 17</div>
                                        <div class="location">Redlands, California</div>
                                        <div class="venue">
                                            <div class="pin">
                                                <img class="svg" src="../img/icon-pin.svg" alt="">
                                            </div>
                                            <div class="address">Splash Kingdom Amphitheater Redlands, CA</div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="schedule-card blue">
                                    <div class="date">
                                        <div class="month">OCT</div>
                                        <div class="day mach">13</div>
                                        <div class="time">6p - 10p</div>
                                    </div>
                                    <div class="info">
                                        <div class="title">Combate Americas</div>
                                        <div class="location">Azteca TV</div>
                                        <div class="venue">
                                            <div class="tv">
                                                <img class="svg" src="../img/icon-tv.svg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- #tab-2 -->
                </div> <!-- .wrapper -->
            </div>
        </section>
    </main>

<?php require 'html-footer.php' ?>