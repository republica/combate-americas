<?php require 'html-header.php' ?>

    <main class="news-page">
        <!-- 
        ******************************
        *
        *   Hero
        *
        ******************************
        -->
        <section class="hero">
            <div class="background">
                <img src="../img/hero.png" alt="">
            </div>
            <div class="heading">
                <div class="wrap">
                    <div class="tag">
                        <div class="caption robo">Combate Clásico</div>
                    </div>
                    <h4 class="robo">Fight Highlights</h4>
                </div>
                <div class="wrap">
                    <h1 class="mach">Ricardo<br>Palacios</h1>
                    <div class="vs white">
                        <div class="mach text">VS</div>
                    </div>
                    <h1 class="mach">Chris Avila</h1>
                </div>
                <div class="wrap">
                    <a href="#" class="btn round yellow">
                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                    </a>
                    <p class="video-text">The biggest event in Combate America's history takes place next July 27th, when the promotions touches down in Mana Wynwood (Miami).</p>
                </div>
            </div>
        </section>
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            
            <div class="sub-content">
                <div class="title white">
                    <h2 class="robo">Trending Now</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>

                <div class="news-wrapper content-slider-wrapper">
                    <ul class="news">
                        <li class="card">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <button class="btn round yellow border small">
                                            <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                        </button>
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="card">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="card">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <button class="btn round yellow border small">
                                            <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                        </button>
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="title white">
                    <h2 class="robo">Upcoming Events</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>

                <div class="news-wrapper content-slider-wrapper">
                    <ul class="news">
                        <li class="card news large">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="card news large">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="card news large">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="title white">
                    <h2 class="robo">Highlights</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>

                <div class="news-wrapper content-slider-wrapper">
                    <ul class="news">
                        <li class="card news large">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="card news large">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="card news large">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="title white">
                    <h2 class="robo">More News</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>

                <div class="news-wrapper content-slider-wrapper">
                    <ul class="news">
                        <li class="card news large">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="card news large">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="card news large">
                            <a href="html-news-single.php">
                                <div class="wrapper">
                                    <div class="image">
                                        <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="title">
                                            <p>Combate 17: El Grito en La Jaula</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div> <!-- .sub-content -->

        </section>
    </main>

<?php require 'html-footer.php' ?>