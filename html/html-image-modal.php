<div class="image-modal-container">
    <div class="overlay"></div>
    <div class="close-btn">
        <img class="svg" src="../img/icon-close.svg" alt="">
    </div>
    <div class="main-content">
        <ul class="bxslider slider-image">
            <li><img src="../img/800x800.jpg" alt=""></li>
            <li><img src="../img/800x800.jpg" alt=""></li>
            <li><img src="../img/800x800.jpg" alt=""></li>
            <li><img src="../img/800x800.jpg" alt=""></li>
            <li><img src="../img/800x800.jpg" alt=""></li>
            <li><img src="../img/800x800.jpg" alt=""></li>
            <li><img src="../img/800x800.jpg" alt=""></li>
        </ul>
        <div id="bx-pager" class="slider-nav">
            <a data-slide-index="0" href=""><img src="../img/800x800.jpg" alt=""></a>
            <a data-slide-index="1" href=""><img src="../img/800x800.jpg" alt=""></a>
            <a data-slide-index="2" href=""><img src="../img/800x800.jpg" alt=""></a>
            <a data-slide-index="3" href=""><img src="../img/800x800.jpg" alt=""></a>
            <a data-slide-index="4" href=""><img src="../img/800x800.jpg" alt=""></a>
            <a data-slide-index="5" href=""><img src="../img/800x800.jpg" alt=""></a>
            <a data-slide-index="6" href=""><img src="../img/800x800.jpg" alt=""></a>
        </div>
    </div>
</div>