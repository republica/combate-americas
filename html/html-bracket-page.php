<?php require 'html-header.php' ?>

    <main class="bracket-challenge-page">
        <!-- 
        ******************************
        *
        *   Hero
        *
        ******************************
        -->
        <section class="hero">
            <div class="background">
                <img src="../img/hero-pre-fight.jpg" alt="">
            </div>
            <div class="heading">
                <div class="wrap">
                    <div class="copa-logo">
                        <img src="../img/logo-copa-combate-bracket-challenge.png" alt="">
                    </div>
                </div>
                <div class="subtext">
                    <p>8 fighters. 1 winner. Create the perfect bracket and you could win a trip to the next Combate Americas event in San Antonio.</p>
                </div>
            </div>
        </section>
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            
            <div class="sub-content brackets challenge">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Build Your Bracket</h2>
                </div>

                <div class="round-container">
                    <ul class="round-nav">
                        <li class="round-link current" data-round="round-1">Round 1</li>
                        <li class="round-link" data-round="round-2">Round 2</li>
                        <li class="round-link" data-round="round-3">Round 3</li>
                        <li class="round-link" data-round="round-4">Winner</li>
                    </ul>
                </div>
                <div class="round-bracket-container">

                    <!-- tooltip -->
                    <div class="tooltip">
                        <div class="container">
                            <p>Let’s get started. To begin, select the winner of each first round fight.</p>
                        </div>
                    </div>
                    <!-- end tooltip -->
                    
                    <div class="rounds">
                        <!-- 
                        *****************
                        *    ROUND 1 
                        ***************** 
                        -->
                        <div id="round-1" class="round round-content current">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                            <!-- 
                            *****************
                            *    FIGHT 2 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                            <!-- 
                            *****************
                            *    FIGHT 3 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                            <!-- 
                            *****************
                            *    FIGHT 4 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <p>john "sexy mexi" castaneda</p>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                        </div> <!-- .round -->

                        <!-- 
                        *****************
                        *    ROUND 2
                        ***************** 
                        -->
                        <div id="round-2" class="round round-content">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <!-- <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <!-- <p>john "sexy mexi" castaneda</p> -->
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <!-- <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <!-- <p>john "sexy mexi" castaneda</p> -->
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                            <!-- 
                            *****************
                            *    FIGHT 2 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <!-- <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <!-- <p>john "sexy mexi" castaneda</p> -->
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <!-- <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <!-- <p>john "sexy mexi" castaneda</p> -->
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                        </div> <!-- .round -->

                        <!-- 
                        *****************
                        *    ROUND 3
                        ***************** 
                        -->
                        <div id="round-3" class="round round-content">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight">

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <!-- <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <!-- <p>john "sexy mexi" castaneda</p> -->
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->

                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <!-- <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <!-- <p>john "sexy mexi" castaneda</p> -->
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->
                            
                        </div> <!-- .round -->

                        <!-- 
                        *****************
                        *    ROUND 4
                        ***************** 
                        -->
                        <div id="round-4" class="round round-content">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight">
                                <div class="bracket-card">
                                    <div class="left">
                                        <div class="image">
                                            <img src="../img/profile-img.png" alt="">
                                        </div>
                                        <div class="details">
                                            <!-- <div class="flag">
                                                <img src="../img/flag.png" alt="">
                                            </div>
                                            <div class="vote">
                                                <img src="../img/icon-vote.png" alt="">
                                            </div>
                                            <div class="pick">
                                                <img src="../img/icon-check.png" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <!-- <p>john "sexy mexi" castaneda</p> -->
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->
                            
                        </div> <!-- .round -->

                    </div> <!-- .rounds -->
                </div> <!-- .round-bracket-container -->
                
            </div> <!-- .sub-content -->

        </section>
    </main>

<?php require 'html-footer.php' ?>