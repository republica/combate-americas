<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../app.css">
	<link rel="stylesheet" type="text/css" href="../style.css">
	<title>Combate Americas</title>
</head>

<body>
    <!-- 
    ******************************
    *
    *   Header
    *
    ******************************
    -->
    <header>
        <div class="logo">
            <h1>
                <a href="html-home.php">
                    <img src="../img/logo.svg" alt="Combate America">
                </a>
            </h1>
        </div>
        <div class="menu">
            <div class="menu-btn">
                <span></span>
            </div>
            <ul class="nav">
                <li><a href="html-fighters.php">Fighters</a></li>
                <li id="watch">
                    <a href="javascript:void(0);">Watch</a>
                    <ul class="watchnav">
                        <li>
                            <div class="logo">
                                <img src="../img/icon-telemundo.png" alt="">
                            </div>
                            <div class="channel-num">441</div>
                            <div class="channel-name">Telemundo</div>
                            <div class="play">
                                <a class="btn round small border yellow" href="#">
                                    <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="logo">
                                <img src="../img/icon-tvazteca.png" alt="">
                            </div>
                            <div class="channel-num">441</div>
                            <div class="channel-name">TV Azteca</div>
                            <div class="play">
                                <a class="btn round small border yellow" href="#">
                                    <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a href="html-schedule.php">Schedule</a></li>
                <li><a href="html-videos.php">Videos</a></li>
                <li><a href="html-news.php">News</a></li>
                <li id="more" class="desktop">
                    <a href="javascript:void(0);">More</a>
                    <ul class="subnav">
                        <li><a href="html-ranking.php">Ranking</a></li>
                        <li><a href="html-about.php">About</a></li>
                        <li><a href="html-contact.php">Contact</a></li>
                    </ul>
                </li>
                <li class="mobile"><a href="html-ranking.php">Ranking</a></li>
                <li class="mobile"><a href="html-about.php">About</a></li>
                <li class="mobile"><a href="html-contact.php">Contact</a></li>
            </ul>
        </div>
        <div class="options">
            <div class="language">
                <p>Español</p>
            </div>
            <div class="search">
                <img src="../img/icon-search.svg" alt="">
            </div>
            <div class="tickets">
                <a href="#" class="btn long yellow">Tickets</a>
            </div>
        </div>
    </header>