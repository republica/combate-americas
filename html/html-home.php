<?php require 'html-header.php' ?>

    <main class="home-page">
        <!-- 
        ******************************
        *
        *   Hero
        *
        ******************************
        -->
        <section class="hero">
            <div class="background">
                <img src="../img/hero.png" alt="">
            </div>
            <div class="heading">
                <div class="wrap">
                    <div class="mach">
                    <h1 class="mach">Ricardo<br>Palacios</h1>
                    <div class="vs white">
                        <div class="mach text">VS</div>
                    </div>
                    <h1 class="mach">Chris Avila</h1>
                    </div>
                </div>
                <div class="wrap">
                    <a href="#" class="btn round yellow">
                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                    </a>
                    <p class="video-text">The biggest event in Combate America's history takes place next July 27th, when the promotions touches down in Mana Wynwood (Miami).</p>

                    <?php require 'html-watch-btn.php' ?>
                </div>
            </div>
        </section>
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            
            <div class="sub-content trending">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Trending Now</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>
                <!-- video container -->
                <ul class="trending-container">
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul> <!-- .trending-container -->
            </div> <!-- .sub-content -->

            <div class="sub-content videos">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Videos</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>
                <!-- video container -->
                <ul class="video-container">
                    <li class="card large">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul> <!-- .video-container -->
            </div> <!-- .sub-content -->

            <div class="sub-content news">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Latest News</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>
                <!-- news container -->
                <ul class="news-container">
                    <li class="card large">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>                        
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul> <!-- .news-container -->
            </div> <!-- .sub-content -->

        </section>
    </main>

<?php require 'html-footer.php' ?>