<?php require 'html-header.php' ?>

    <main class="home-page">
        <!-- 
        ******************************
        *
        *   Hero
        *
        ******************************
        -->
        <section class="hero hero-alt">
            <div class="background">
                <img src="../img/hero-alt/bg.jpg" alt="">
            </div>
            <div class="hero-info">
                <div class="title-text">
                    <img src="../img/hero-alt/title-text.png" alt="Queen Warriors">
                </div>
                <div class="logos">
                    <img class="ticketmaster" src="../img/hero-alt/logo-ticketmaster.png" alt="Ticketmaster logo">
                    <img class="freeman" src="../img/hero-alt/logo-freeman.png" alt="Freeman logo">
                    <img class="telemundo" src="../img/hero-alt/logo-telemundo.png" alt="Telemundo logo">
                </div>
            </div>
        </section>
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            
            <div class="sub-content trending">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Trending Now</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>
                <!-- video container -->
                <ul class="trending-container">
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul> <!-- .trending-container -->
            </div> <!-- .sub-content -->

            <div class="sub-content videos">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Videos</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>
                <!-- video container -->
                <ul class="video-container">
                    <li class="card large">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <button class="btn round yellow border small">
                                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                    </button>
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul> <!-- .video-container -->
            </div> <!-- .sub-content -->

            <div class="sub-content news">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Latest News</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>
                <!-- news container -->
                <ul class="news-container">
                    <li class="card large">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>                        
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="card">
                        <a href="">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <p>Combate 17: El Grito en La Jaula</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul> <!-- .news-container -->
            </div> <!-- .sub-content -->

        </section>
    </main>

<?php require 'html-footer.php' ?>