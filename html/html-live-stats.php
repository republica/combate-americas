<?php require 'html-header.php' ?>

    <main class="home-page">
        <!-- 
        ******************************
        *
        *   Hero
        *
        ******************************
        -->
        <section class="hero">
            <div class="background">
                <img src="../img/hero.png" alt="">
            </div>
            <div class="heading">
                <div class="wrap">
                    <div class="mach">
                    <h1 class="mach">Ricardo<br>Palacios</h1>
                    <div class="vs white">
                        <div class="mach text">VS</div>
                    </div>
                    <h1 class="mach">Chris Avila</h1>
                    </div>
                </div>
                <div class="wrap">
                    <a href="#" class="btn round yellow">
                        <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                    </a>
                    <p class="video-text">The biggest event in Combate America's history takes place next July 27th, when the promotions touches down in Mana Wynwood (Miami).</p>

                    <?php require 'html-watch-btn.php' ?>
                </div>
            </div>
        </section>
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">

            <div class="sub-content live-stats">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Live Stats</h2>
                    <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                </div>

                <div class="white-card">
                    <div class="info">
                        <div class="main-title">
                            <h2 class="mach">Combate 17</h2>
                        </div>
                        <div class="date">
                            <p>09.15.2017 - Redlands, California</p>
                        </div>
                        <div class="location">
                            <p>Splash Kingdom Ampitheater<br>Redlands, CA</p>
                        </div>
                        <a class="btn long yellow" href="">Find Tickets</a>
                    </div>
                    <div class="image">
                        <img src="../img/fight-analytics-image.png" alt="">
                    </div>
                </div>

                <!-- stats card 1 -->

                <a href="#fight-1" class="stats-card open-modal" rel="modal:open">
                    <div class="image-left">
                        <img src="../img/temp/headshots/head-1.png" alt="">
                    </div>
                    <div class="stats">
                        <div class="name">
                            <p>jose</p>
                            <h3 class="mach">estrada</h3>
                        </div>
                        <div class="vs white">
                            <div class="mach text">VS</div>
                        </div>
                        <div class="name">
                            <p>izic</p>
                            <h3 class="mach">fernandez</h3>
                        </div>
                        <div class="info">
                            <p><strong>DEC. — Round 5</strong></p>
                        </div>
                    </div>
                    <div class="image-right">
                        <img src="../img/temp/headshots/head-2.png" alt="">
                    </div>
                </a>

                <!-- stats card 2 -->
                <a href="#" class="stats-card">
                    <div class="image-left">
                        <img src="../img/temp/headshots/head-3.png" alt="">
                    </div>
                    <div class="stats">
                        <div class="name">
                            <p>jose</p>
                            <h3 class="mach">estrada</h3>
                        </div>
                        <div class="vs white">
                            <div class="mach text">VS</div>
                        </div>
                        <div class="name">
                            <p>izic</p>
                            <h3 class="mach">fernandez</h3>
                        </div>
                        <div class="info">
                            <p><strong>DEC. — Round 5</strong></p>
                        </div>
                    </div>
                    <div class="image-right">
                        <img src="../img/temp/headshots/head-4.png" alt="">
                    </div>
                </a>

                <!-- stats card 3 -->
                <a href="#" class="stats-card">
                    <div class="image-left">
                        <img src="../img/temp/headshots/head-5.png" alt="">
                    </div>
                    <div class="stats">
                        <div class="name">
                            <p>jose</p>
                            <h3 class="mach">estrada</h3>
                        </div>
                        <div class="vs white">
                            <div class="mach text">VS</div>
                        </div>
                        <div class="name">
                            <p>izic</p>
                            <h3 class="mach">fernandez</h3>
                        </div>
                        <div class="info">
                            <p><strong>DEC. — Round 5</strong></p>
                        </div>
                    </div>
                    <div class="image-right">
                        <img src="../img/temp/headshots/head-6.png" alt="">
                    </div>
                </a>

                <!-- stats card 4 -->
                <a href="#" class="stats-card">
                    <div class="image-left">
                        <img src="../img/temp/headshots/head-7.png" alt="">
                    </div>
                    <div class="stats">
                        <div class="name">
                            <p>jose</p>
                            <h3 class="mach">estrada</h3>
                        </div>
                        <div class="vs white">
                            <div class="mach text">VS</div>
                        </div>
                        <div class="name">
                            <p>izic</p>
                            <h3 class="mach">fernandez</h3>
                        </div>
                        <div class="info">
                            <p><strong>DEC. — Round 5</strong></p>
                        </div>
                    </div>
                    <div class="image-right">
                        <img src="../img/temp/headshots/head-8.png" alt="">
                    </div>
                </a>

                <!-- stats card 5 -->
                <a href="#" class="stats-card">
                    <div class="image-left">
                        <img src="../img/temp/headshots/head-1.png" alt="">
                    </div>
                    <div class="stats">
                        <div class="name">
                            <p>jose</p>
                            <h3 class="mach">estrada</h3>
                        </div>
                        <div class="vs white">
                            <div class="mach text">VS</div>
                        </div>
                        <div class="name">
                            <p>izic</p>
                            <h3 class="mach">fernandez</h3>
                        </div>
                        <div class="info">
                            <p><strong>DEC. — Round 5</strong></p>
                        </div>
                    </div>
                    <div class="image-right">
                        <img src="../img/temp/headshots/head-2.png" alt="">
                    </div>
                </a>
                
                <!-- stats card 6 -->
                <a href="#" class="stats-card">
                    <div class="image-left">
                        <img src="../img/temp/headshots/head-3.png" alt="">
                    </div>
                    <div class="stats">
                        <div class="name">
                            <p>jose</p>
                            <h3 class="mach">estrada</h3>
                        </div>
                        <div class="vs white">
                            <div class="mach text">VS</div>
                        </div>
                        <div class="name">
                            <p>izic</p>
                            <h3 class="mach">fernandez</h3>
                        </div>
                        <div class="info">
                            <p><strong>DEC. — Round 5</strong></p>
                        </div>
                    </div>
                    <div class="image-right">
                        <img src="../img/temp/headshots/head-4.png" alt="">
                    </div>
                </a>

                <!-- stats card 7 -->
                <a href="#" class="stats-card">
                    <div class="image-left">
                        <img src="../img/temp/headshots/head-5.png" alt="">
                    </div>
                    <div class="stats">
                        <div class="name">
                            <p>jose</p>
                            <h3 class="mach">estrada</h3>
                        </div>
                        <div class="vs white">
                            <div class="mach text">VS</div>
                        </div>
                        <div class="name">
                            <p>izic</p>
                            <h3 class="mach">fernandez</h3>
                        </div>
                        <div class="info">
                            <p><strong>DEC. — Round 5</strong></p>
                        </div>
                    </div>
                    <div class="image-right">
                        <img src="../img/temp/headshots/head-6.png" alt="">
                    </div>
                </a>

                <!-- stats card 8 -->
                <a href="#" class="stats-card">
                    <div class="image-left">
                        <img src="../img/temp/headshots/head-7.png" alt="">
                    </div>
                    <div class="stats">
                        <div class="name">
                            <p>jose</p>
                            <h3 class="mach">estrada</h3>
                        </div>
                        <div class="vs white">
                            <div class="mach text">VS</div>
                        </div>
                        <div class="name">
                            <p>izic</p>
                            <h3 class="mach">fernandez</h3>
                        </div>
                        <div class="info">
                            <p><strong>DEC. — Round 5</strong></p>
                        </div>
                    </div>
                    <div class="image-right">
                        <img src="../img/temp/headshots/head-8.png" alt="">
                    </div>
                </a>

                

            </div>

        </section>
    </main>

    <div id="fight-1" class="main-modal" style="display:none;">
        <div class="close-btn">
            <a href="#" rel="modal:close">
                <img class="svg" src="../img/icon-close.svg" alt="">
            </a>
        </div>
        <div class="stats-container">
            <div class="wrapper">
                <ul class="tabs">
                    <li class="tab-link current" data-tab="tab-1">Tale of the tape</li>
                    <li class="tab-link" data-tab="tab-2">Live Stats</li>
                </ul>

                <div id="tab-1" class="tab-content current">
                    <table>
                        <tr>
                            <td>29</td>
                            <td>Age</td>
                            <td>26</td>
                        </tr>
                        <tr>
                            <td>5'7"</td>
                            <td>Height</td>
                            <td>5'5"</td>
                        </tr>
                        <tr>
                            <td>N/A</td>
                            <td>Reach</td>
                            <td>2'2"</td>
                        </tr>
                        <tr>
                            <td>145.0</td>
                            <td>Weight</td>
                            <td>145.0 lbs</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>Wins</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Losses</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>0</td>
                            <td>Draws</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>Argentina</td>
                            <td>City</td>
                            <td>Nuevo Leon</td>
                        </tr>
                        <tr>
                            <td>N/A</td>
                            <td>Style</td>
                            <td>Untraditional</td>
                        </tr>
                        <tr>
                            <td>Entram</td>
                            <td>Team</td>
                            <td>Lions Alpha</td>
                        </tr>
                    </table>
                </div>
                <div id="tab-2" class="tab-content">
                    <table>
                        <tr>
                            <td>67</td>
                            <td>strikes thrown</td>
                            <td>88</td>
                        </tr>
                        <tr>
                            <td>18</td>
                            <td>SIGNIFICANT STRIKES LANDED</td>
                            <td>23</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>TAKEDOWNS ATTEMPTED</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>TAKEDOWNS</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>TRANSITIONS</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>SUBMISSIONS ATTEMPTS</td>
                            <td>4</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>TAKEDOWNS DEFENDED</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>CAGE CONTROL</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>GROUND CONTROL</td>
                            <td>2</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php require 'html-footer.php' ?>