<?php require 'html-header.php' ?>

    <main class="news-single-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="sub-content">
                <div class="wrapper">
                    <div class="main">
                        <h4 class="mach title">Rojo VS Molina July 27</h4>
                        <!-- <a href="#"> -->
                            <div class="video-img">
                                <img src="../img/800x800.jpg" alt="">
                            </div>
                        <!-- </a> -->
                        
                        <div class="info">
                            <p>Combate Americas is going old school.</p>
                            <p>The first Hispanic-based promotion announced today that it will host a Copa Combate one-night, eight-man tournament in which the winner will take home a $100,000 prize. The event is set to take place Nov. 11 in Cancun, Mexico.</p>
                            <p>The tournament will feature bantamweights from the U.S., Latin America and Spain, though the promotion has yet to announce the field. A Combate Americas official revealed some competition rules to MMAjunkie:</p>
                            <ul>
                                <li>Quarterfinal and semifinal bouts will be scheduled for three 3-minute rounds.</li>
                                <li>The championship final will be scheduled for three 5-minute rounds.</li>
                                <li>There will be two tournament alternate bouts.</li>
                                <li>No elbows will be permitted in the quarterfinal or semifinal bouts.</li>
                                <li>Elbows will be permitted during the championship final.</li>
                            </ul>
                            <blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit</blockquote>
                            <hr>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas fringilla phasellus faucibus scelerisque. Suspendisse in est ante in nibh mauris cursus mattis. Dignissim convallis aenean et tortor at risus viverra adipiscing. Odio ut enim blandit volutpat maecenas.</p>
                            <p>Et pharetra pharetra massa massa ultricies mi. Risus nec feugiat in fermentum posuere. Arcu dui vivamus arcu felis bibendum ut tristique et egestas. Ante in nibh mauris cursus mattis molestie a. Elementum integer enim neque volutpat ac tincidunt vitae semper quis. Arcu felis bibendum ut tristique et egestas quis ipsum. Orci phasellus egestas tellus rutrum tellus. Proin sagittis nisl rhoncus mattis rhoncus urna. Sit amet porttitor eget dolor morbi non arcu.</p>
                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                            <p>Copa Combate is set to happen one day before the 24-year anniversary of UFC 1, also a one-night tournament that took place Nov. 12, 1993 at McNichols Sports Arena in Denver. Royce Gracie won three fights by submission that night to claim the $50,000 grand prize.</p>
                        </div>
                        <ul class="tags">
                            <li><h4>Tags:</h4></li>
                            <li><a href="">rojo</a></li>
                            <li><a href="">molina</a></li>
                            <li><a href="">combate clasico</a></li>
                            <li><a href="">event</a></li>
                            <li><a href="">highlights</a></li>
                            <li><a href="">rojo</a></li>
                        </ul>
                    </div> <!-- .main -->

                    <div class="sidebar">
                        <div class="ad">
                            <img src="../img/banners/728x90.jpg" alt="">
                        </div>

                        <div class="title white">
                            <h2 class="robo">Related Articles</h2>
                            <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                        </div>
                        <ul class="related-news">
                            <li class="card">
                                <a href="html-news-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                            <li class="card">
                                <a href="html-news-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                            <li class="card">
                                <a href="html-news-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                        </ul>

                        <div class="title white">
                            <h2 class="robo">Sponsor Video</h2>
                            <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                        </div>

                        <div class="sponsor-vids">
                            <div class="card">
                                <a href="html-news-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div> <!-- .card -->
                        </div>

                        <div class="title white">
                            <h2 class="robo">More News</h2>
                            <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                        </div>

                        <ul class="related-news">
                            <li class="card">
                                <a href="html-news-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                            <li class="card">
                                <a href="html-news-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                            <li class="card">
                                <a href="html-news-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                        </ul>

                    </div> <!-- .sidebar -->
                    
                </div> <!-- .wrapper -->
            </div> <!-- .sub-content -->
        </section>
    </main>

<?php require 'html-footer.php' ?>