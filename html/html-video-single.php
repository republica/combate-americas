<?php require 'html-header.php' ?>

    <main class="video-single-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="sub-content">
                <div class="wrapper">
                    <div class="main">
                        <!-- <a href="#"> -->
                            <div class="video-img">
                                <button class="btn round yellow">
                                    <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                </button>
                                <img src="../img/800x800.jpg" alt="">
                            </div>
                        <!-- </a> -->
                        <h4 class="mach title">Rojo VS Molina July 27</h4>
                        <div class="info">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <ul class="tags">
                            <li><h4>Tags:</h4></li>
                            <li><a href="">rojo</a></li>
                            <li><a href="">molina</a></li>
                            <li><a href="">combate clasico</a></li>
                            <li><a href="">event</a></li>
                            <li><a href="">highlights</a></li>
                            <li><a href="">rojo</a></li>
                        </ul>
                    </div> <!-- .main -->

                    <div class="sidebar">
                        <div class="ad">
                            <img src="../img/banners/728x90.jpg" alt="">
                        </div>

                        <div class="title white">
                            <h2 class="robo">Related Videos</h2>
                            <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                        </div>
                        <ul class="related-vids">
                            <li class="card">
                                <a href="html-video-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                            <li class="card">
                                <a href="html-video-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                            <li class="card">
                                <a href="html-video-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                        </ul>

                        <div class="title white">
                            <h2 class="robo">Sponsor Video</h2>
                            <span><img class="svg" src="../img/icon-arrow.svg" alt=""></span>
                        </div>

                        <div class="sponsor-vids">
                            <div class="card">
                                <a href="html-video-single.php">
                                    <div class="wrapper">
                                        <div class="image">
                                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p>Combate 17: El Grito en La Jaula</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div> <!-- .card -->
                        </div>
                    </div> <!-- .sidebar -->
                    
                </div> <!-- .wrapper -->
            </div> <!-- .sub-content -->
        </section>
    </main>

<?php require 'html-footer.php' ?>