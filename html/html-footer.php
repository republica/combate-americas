    <?php require 'html-popup-modal.php' ?>
    <?php require 'html-image-modal.php' ?>
    <?php require 'html-search-modal.php' ?>
    <!-- 
    ******************************
    *
    *   Subscribe
    *
    ******************************
    -->
    <section class="subscribe">
         <div class="wrapper">
             <h3 class="robo">Subscribe to our Newsletter</h3>
             <div class="input-wrapper">
                 <input type="text" placeholder="Enter your email address">
                 <button class="btn long yellow">Subscribe</button>
             </div>
         </div>
    </section>

    <!-- 
    ******************************
    *
    *   Footer
    *
    ******************************
    -->
    
    <footer>
        <div class="wrapper">
            <ul class="nav">
                <li><a href="">Fighters</a></li>
                <li><a href="">Schedule</a></li>
                <li><a href="">Tickets</a></li>
                <li><a href="">Videos</a></li>
                <li><a href="">News</a></li>
                <li><a href="">About</a></li>
                <li><a href="">Rankings</a></li>
                <li><a href="">Terms of Use</a></li>
                <li><a href="">Privacy Policy</a></li>
                <li><a href="">Contact</a></li>
            </ul>
            <div class="social">
                <div class="title">
                    <h3 class="robo desktop">Follow Us</h3>
                </div>
                <ul class="links">
                    <li><a href=""><img src="../img/icon-fb.svg" alt=""></a></li>
                    <li><a href=""><img src="../img/icon-tw.svg" alt=""></a></li>
                    <li><a href=""><img src="../img/icon-ig.svg" alt=""></a></li>
                    <li><a href=""><img src="../img/icon-yt.svg" alt=""></a></li>
                </ul>
            </div>
        </div>
    </footer>

	<script src="../app.js"></script>
	<script src="../js/scripts.js"></script>

</body>

</html>