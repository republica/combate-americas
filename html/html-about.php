<?php require 'html-header.php' ?>

    <main class="about-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="main-heading">
                <div class="background">
                    <img src="../img/hero-about.png" alt="">
                </div>
                <div class="wrapper">
                    <h1 class="mach">Combate<br>Americas</h1>
                    <h4>THE 1ST FIRST U.S. HISPANIC MMA SPORTS FRANCHISE IN HISTORY</h4>
                </div>
            </div>
            <div class="sub-content">
                <div class="wrapper">
                    <p>Combate Americas, LLC. is the first U.S. Hispanic Mixed Martial Arts (MMA) sports franchise in history, designed to build Latino fighting champions and serve Hispanic fans, one of the world's most avid groups of prize fighting enthusiasts.</p><p>The Combate Americas franchise includes award winning reality TV programming; action-packed, world championship live events and mobile programming.  Following its first season of reality TV on NBC Universo (formerly mun2), the company won the “Best Variety or Reality Show” at the 29th annual Imagen Awards on August 1, 2014, besting Mark Burnett’s The Voice for the coveted prize.  On October 14, 2014, Combate Americas scored the “Best Show or Series Reality/Competition/Game Show” honor at the 2014 Cablefax Awards.</p><p>The company's CEO, Campbell McLaren, is universally recognized as the co-founder/co-creator of the Ultimate Fighting Championship (UFC).  New York Magazine described McLaren as "the marketing genius behind the UFC" and Yahoo! Sports proclaimed that he "knows more about the sport than just about anyone in it today."</p><p>With an unprecedented product and a blue-chip ownership and management team, including reality TV pioneers Bunim/Murray Productions and Major League Baseball Advanced Media, Combate Americas is poised to break new ground and bring about a new era in world championship level MMA competition.</p>
                    <div class="employee-wrap">
                        <img src="../img/employee-1.png" alt="">
                        <div class="info">
                            <h2 class="mach">Campbell Mclaren</h2>
                            <h3>Founder + CEO</h3>
                        </div>
                    </div>
                    <p>Campbell McLaren brings extraordinary acumen and experience to COMBATE AMERICAS.  As the acknowledged creator of The Ultimate Fighting Championship, he has been called “the brains behind The UFC” (New York Magazine) and Yahoo Sports said “…he knows more about the sport than just about anyone in it today.”</p><p>McLaren also created The Iron Ring, BET’s hip-hop version of The UFC that quickly became a hit show.  The Iron Ring starred rappers Ludacris, TI, Nelly, and boxer Floyd Mayweather and had BET’s fourth highest premiere in the network's history.</p><p>McLaren has also produced many highly rated and successful large-scale Television specials, PPV events and series, including VH1’s Paul McCartney’s Tribute to Linda, Lifetime Television’s Free to Laugh, and Court TV’s Major Misbehavior and The Wrong Man.  McLaren’s television work has been awarded International Television Awards, Cable Ace Awards, and has been shown at SXSW, Sundance Film Festival, and the Berlin Film Festival.</p><p>McLaren studied documentary filmmaking with Richard Leacock at the Massachusetts Institute of Technology and has an A.B. from the University of California, Berkeley.</p>
                    <div class="employee-wrap">
                        <img src="../img/employee-2.png" alt="">
                        <div class="info">
                            <h2 class="mach">Andrea Calle</h2>
                            <h3></h3>
                        </div>
                    </div>
                    <p>Andrea Calle is one of the most downloaded figures in sports and entertainment, with over one million downloads, winning “Best MMA Personality” in 2012 and 2013. She co-hosted the first season of Combate Americas, which won the Imagen Award for “Best Variety or Reality Show.” She is also the lead reporter for Autoridad MMA (formerly known as El Octagono) the leading broadcasting company in Mixed Martial Arts in Spanish, awarded 2012’s “Best Internet Media Source.”</p><p>She was featured on Telemundo, Univision’s Escandalo TV, BET’s Spring Bling, and ABC’s The T.O. Show.</p><p>She has worked as a model for various designers, music videos, and entertainment magazines. She has also appeared as a red carpet host for multiple entertainment events, and as a NPC fitness competitor in the bikini division.</p><p>Andrea has been recognized for her acting as well, winning the “Best Actress” award in the 2013 Christian film festival yesHEis for her main role in the short film Una Mano Amiga.</p><p>Andrea was born in Medellin, Colombia and moved to the United States in 1998. She is a graduate of the University of Miami, currently pursuing certification in fitness and nutrition.</p>
                </div> <!-- .wrapper -->
            </div>
        </section>
    </main>

<?php require 'html-footer.php' ?>