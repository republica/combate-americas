<div class="search-container">
    <div class="overlay"></div>
    <div class="main-content">
        <div class="search-section">
            <div class="close-btn">
                <img src="../img/icon-close.svg" alt="">
            </div>
            <h3 class="title">Search Combate Americas</h3>
            <div class="input-wrapper">
                <input type="text" placeholder="Search">
                <button class="btn round yellow">
                    <img class="svg" src="../img/icon-search.svg" alt="">
                </button>
            </div>
            <p class="result-num">43 Results Found</p>
        </div>
        <div class="results">
            <ul class="result-container">
                <li>
                    <a href="">
                        <div class="image">
                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                        </div>
                        <div class="info">
                            <h4 class="title">Jose Estrada "Froggy"</h4>
                            <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="">
                        <div class="image">
                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                            <button class="btn round yellow border">
                                <img class="svg" src="../img/icon-play-arrow.svg" alt="">
                            </button>
                        </div>
                        <div class="info">
                            <h4 class="title">Jose Estrada "Froggy"</h4>
                            <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="">
                        <div class="image">
                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                        </div>
                        <div class="info">
                            <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                            <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="">
                        <div class="image">
                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                        </div>
                        <div class="info">
                            <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                            <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="">
                        <div class="image">
                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                        </div>
                        <div class="info">
                            <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                            <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="">
                        <div class="image">
                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                        </div>
                        <div class="info">
                            <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                            <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="">
                        <div class="image">
                            <img src="../img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                        </div>
                        <div class="info">
                            <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                            <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>