<?php require 'html-header.php' ?>

    <main class="contact-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">
            <div class="main-heading">
                <div class="background">
                    <img src="../img/hero.png" alt="">
                </div>
                <div class="wrapper">
                    <h1 class="mach">Contact</h1>
                </div>
            </div>
            <div class="sub-content">
                <div class="wrapper">
                    <div class="contact-section">
                        <div class="form">
                            <input type="text" placeholder="First Name">
                            <input type="text" placeholder="Last Name">
                            <input type="text" placeholder="Email Address">
                            <input type="text" placeholder="Subject">
                            <textarea name="" id="" placeholder="Message"></textarea>
                            <button class="btn long yellow">Send</button>
                        </div>
                        <div class="info">
                            <div class="address">
                                <p>385 Annabell Blvd,  Suite 178<br>Los Angeles, CA 78632</p>
                            </div>
                        </div>
                    </div>
                </div> <!-- .wrapper -->
            </div>
        </section>
    </main>

<?php require 'html-footer.php' ?>