<?php get_header(); ?>

<main class="video-single-page news-single-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
    -->
    <section class="content">
        <div class="sub-content">
            <div class="wrapper">
                <div class="main">
                    <h1 class="mach title"><?php the_title(); ?></h1>
                    <video controls width="100%" poster="<?php echo get_the_post_thumbnail_url() ?>">
                        <source src="<?php the_field('video_url'); ?>" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <div class="video-img">
                  <!--               <button class="btn round yellow">
                                    <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-play-arrow.svg" alt="">
                                </button> -->
                                
                            </div>

                            <div class="info">
                                <?php the_content(); ?>
                            </div>
                            <?php the_tags( '<ul class="tags"><li><h4>Tags:</h4></li><li>', '</li><li>', '</li></ul>' ); ?>
                        </div> <!-- .main -->

                        <div class="sidebar">

                            <div class="ad">
                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/banners/728x90.jpg" alt="">
                            </div>

                        <?php $relatedVideos = get_field('related_videos'); ?>
                        <?php if ($relatedVideos) { ?>
                        <div class="title white">
                            <h2 class="robo"><?php echo __('Related Videos'); ?></h2>
                            <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
                        </div>
                        <?php
                        $exclude = array();
                        $exclude[] = $post->ID;
                        //var_dump(expression);
                        $relatedVideos = array_diff($relatedVideos, $exclude);
                        $args = array(
                            'post_type'         => 'video',
                            'post__in'          => $relatedVideos,
                            'posts_per_page'    => 3,
                            'orderby'           => 'date',
                            'order'             => 'DESC',
                            ); ?>

                        <?php query_posts($args); 
                        if (have_posts()) :
                            while (have_posts()) : the_post(); ?>                                            
                            <li class="card">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <div class="wrapper">
                                        <div class="image">
                                            <?php the_post_thumbnail( 'medium' ) ?>
                                        </div>
                                        <div class="info">
                                            <button class="btn round yellow border small">
                                                <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-play-arrow.svg" alt="">
                                            </button>
                                            <div class="title">
                                                <p><?php the_title(); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                        <?php endwhile;
                    endif; wp_reset_query(); 
                    ?>
                </ul>
                <?php } ?>

                        </div> <!-- .sidebar -->
                        
                    </div> <!-- .wrapper -->
                </div> <!-- .sub-content -->
            </section>
        </main>

        <?php get_footer(); ?>