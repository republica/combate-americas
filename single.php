<?php get_header(); ?>

<main class="news-single-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
    -->
    <section class="content">
        <div class="sub-content">
            <div class="wrapper">
                <div class="main">
                    <h1 class="mach title"><?php the_title(); ?></h1>
                    <!-- <a href="#"> -->
                        <div class="video-img">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <!-- </a> -->
                        
                        <div class="info">
                            <?php the_content(); ?>
                        </div>
                        <?php the_tags( '<ul class="tags"><li><h4>Tags:</h4></li><li>', '</li><li>', '</li></ul>' ); ?>
                    </div> <!-- .main -->

                    <div class="sidebar">
                        <div class="ad">
                            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/banners/728x90.jpg" alt="">
                        </div>

                        <?php $relatedNews = get_field('related_news'); ?>
                        <?php if ($relatedNews) { ?>
                        <div class="title white">
                            <h2 class="robo"><?php echo __('Related News'); ?></h2>
                            <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
                        </div>
                        <?php
                        $args = array(
                            'post_type'         => 'post',
                            'post__in'          => $relatedNews,
                            'posts_per_page'    => 3,
                            'orderby'           => 'date',
                            'order'             => 'DESC',
                            ); ?>

                        <?php query_posts($args); 
                        if (have_posts()) :
                            while (have_posts()) : the_post(); ?>                                            
                            <li class="card">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <div class="wrapper">
                                        <div class="image">
                                            <?php the_post_thumbnail( 'medium' ) ?>
                                        </div>
                                        <div class="info">
                                              <div class="title">
                                                <p><?php the_title(); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li> <!-- .card -->
                        <?php endwhile;
                    endif; wp_reset_query(); 
                    ?>
                </ul>
                <?php } ?>

            </div> <!-- .sidebar -->

        </div> <!-- .wrapper -->
    </div> <!-- .sub-content -->
</section>
</main>


<?php get_footer(); ?>