<?php get_header(); ?>


<main class="fighter-single-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
    -->
    <section class="content">
        <div class="main-heading single-fighter">
            <ul class="fighter-intro-container">
                <li>
                    <div class="fighter-intro">
                        <?php
                        $fighter_facebook = get_field('fighter_facebook');
                        $fighter_twitter = get_field('fighter_twitter');
                        $fighter_instagram = get_field('fighter_instagram');
                        $fighter_youtube = get_field('fighter_youtube');
                        if ($fighter_facebook || $fighter_twitter || $fighter_instagram || $fighter_youtube) {
                            ?>
                            <div class="social-media">
                                <ul class="icons">
                                    <?php if ($fighter_facebook) { ?><li><a href="https://www.facebook.com/<?php echo $fighter_facebook; ?>" title="Facebook" target="_blank"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icons-fighter/fb.svg" alt="Logo Facebook"></a></li><?php } ?>
                                    <?php if ($fighter_twitter) { ?><li><a href="https://twitter.com/<?php echo $fighter_twitter; ?>" title="Twitter" target="_blank"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icons-fighter/tw.svg" alt="Logo Twitter"></a></li><?php } ?>
                                    <?php if ($fighter_instagram) { ?><li><a href="https://instagram.com/<?php echo $fighter_instagram; ?>" title="Instagram" target="_blank"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icons-fighter/ig.svg" alt="Logo Instagram"></a></li><?php } ?>
                                    <?php if ($fighter_youtube) { ?><li><a href="https://www.youtube.com/channel/<?php echo $fighter_youtube; ?>" title="Youtube" target="_blank"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icons-fighter/yt.svg" alt="Logo Youtube"></a></li><?php } ?>
                                </ul>
                            </div>
                            <?php } ?>
                            <div class="wrapper">
                                <div class="background">
                                    <?php the_post_thumbnail(); ?>
                                </div>
                                <div class="info">
                                    <div class="info-top">
                                        <div class="name">
                                            <h4 class="robo"><?php the_field('fighter_first_name'); ?></h4>
                                            <h1 class="mach"><?php the_field('fighter_last_name'); ?></h1>
                                            <span>"<?php the_field('fighter_alias'); ?>"</span>
                                        </div>
                                        <div class="weight-class">
                                            <p><?php the_terms( $post->ID, 'weight-class', '', ' / ' ); ?></p>
                                            <?php $country = wp_get_post_terms($post->ID, 'country', array("fields" => "all"));?>
                                            <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>">
                                        </div>
                                    </div>
                                    <?php $proRecord = explode('-', str_replace(' ', '', get_field('fighter_pro_record'))); ?>
                                    <div class="info-bottom-alt">
                                        <div class="wins">
                                            <h3 class="mach"><?php echo $proRecord[0]; ?></h3>
                                            <p><?php echo __('Wins'); ?></p>
                                        </div>
                                        <div class="losses">
                                            <h3 class="mach"><?php echo $proRecord[1]; ?></h3>
                                            <p><?php echo __('Losses'); ?></p>
                                        </div>
                         <!--            <button class="btn round yellow">
                                        <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-play-arrow.svg" alt="">
                                    </button>
                                    (Pending) -->
                                </div>
                            </div>
                        </div> <!-- .wrapper -->
                    </div> <!-- .fighter-intro -->
                    <div class="fighter-stats">
                        <div class="wrapper">
                            <ul class="tabs">
                                <li class="tab-link current" data-tab="tab-1"><?php echo __('Details'); ?></li>
                                <li class="tab-link" data-tab="tab-2"><?php echo __('Highlights'); ?></li>
                                <li class="tab-link" data-tab="tab-3"><?php echo __('Bio'); ?></li>
                            </ul>

                            <div id="tab-1" class="tab-content current">
                                <table>
                                    <tr>
                                        <td><?php echo __('Age'); ?></td>
                                        <td>
                                            <?php 
                                            $birthdate = get_field('fighter_date_of_birth');
                                            echo  $age = date_diff(date_create($birthdate), date_create('now'))->y;;
                                            ?>                                                 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Date of Birth'); ?></td>
                                        <td><?php echo $birthdate; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Last Weigh-In'); ?></td>
                                        <td><?php the_field('fighter_fighting_weight'); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Height'); ?></td>
                                        <td><?php the_field('fighter_height'); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Reach'); ?></td>
                                        <td><?php the_field('fighter_reach'); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Born'); ?></td>
                                        <td><?php the_terms( $post->ID, 'country', '', ' / ' ); ?></td>
                                    </tr>                    
                                </table>
                            </div>
                            <div id="tab-2" class="tab-content">
                                <?php the_field('fighter__headlines'); ?>
                            </div>
                            <div id="tab-3" class="tab-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div> <!-- .fighter-stats -->
                </li>
            </ul>
        </div> <!-- .heading --> 


        <?php 

        $images = get_field('fighter_photos_gallery');
        $size = 'full';
        
        if( $images ): ?>
        <div class="sub-content pictures">

            <div class="title white">
                <h2 class="robo"><?php echo __('Pictures'); ?></h2>
                <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
            </div>
            <div class="pic-wrapper">
                <ul class="pictures-container">
                    <?php foreach( $images as $image ): ?>
                        <li>
                            <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>

    <?php $relatedVideos = get_field('videos_fighters'); ?>
    <?php if ($relatedVideos) { ?>
    <div class="sub-content videos">
        <div class="title white">
            <h2 class="robo"><?php echo __("Related videos"); ?></h2>
            <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
        </div>
        <div class="vid-wrapper content-slider-wrapper">
            <ul class="videos content-slider">
                <?php
                $args = array(
                    'post_type'         => 'video',
                    'post__in'          => $relatedVideos,
                    'posts_per_page'    => 5,
                    'orderby'           => 'date',
                    'order'             => 'DESC',
                );
                query_posts($args); 
                $i = 0;
                if (have_posts()) :
                    while (have_posts()) : the_post();  $i++; if ($i == 1) $cardClass = 'large'; 
                        include( locate_template( 'parts/video-card.php', false, false ) );
                    endwhile;
                endif; wp_reset_query(); 
                ?>
            </ul>
        </div>
    </div>
    <?php } ?>
    <div class="sub-content news">
        <?php $relatedNews = get_field('news_fighters'); ?>
        <?php if ($relatedNews) { ?>
        <div class="title white">
            <h2 class="robo"><?php echo __("Related news"); ?></h2>
            <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
        </div>
        <div class="news-wrapper content-slider-wrapper">
            <ul class="news content-slider">
                <?php
                $args = array(
                    'post_type'         => 'post',
                    'post__in'          => $relatedNews,
                    'posts_per_page'    => 5,
                    'orderby'           => 'date',
                    'order'             => 'DESC',
                    ); ?>
                <?php query_posts($args); 
                if (have_posts()) :
                    while (have_posts()) : the_post();
                        include( locate_template( 'parts/article-card.php', false, false ) );
                    endwhile;
                endif; wp_reset_query(); 
                ?>
            </ul>
        </div>
        <?php } ?>
    </div>

</section>
</main>


<?php get_footer(); ?>