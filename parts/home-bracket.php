<?php 
$args = array(
    'post_type'     => 'bracket',
    'post__in'      => $bracket,
);
query_posts($args); 
if (have_posts()) : ?>
<?php while (have_posts()) { the_post();  ?>
<?php 
$bracketRound1       = get_field('bracket_round_1');
$bracketRound2       = get_field('bracket_round_2');
$bracketRoundFinal   = get_field('bracket_round_final');
$winners             = [];
?>
<div class="sub-content brackets">
    <!-- section title -->
    <div class="title white">
        <h2 class="robo"><?php echo __('TOURNAMENT BRACKET'); ?></h2>
        <?php if (get_field('is_live')) { ?>
        <a href="<?php echo SITE_URL; ?>/copa-combate-live-stats/" title="<?php echo __('Stats'); ?>" class="btn long yellow cta-bracket-challenge"><h4><?php echo __('Stats'); ?></h4></a>
        <?php } else { ?><?php if (!get_field('close_challenge')) { ?><a href="<?php the_field('bracket_challenge_url'); ?>" title="<?php echo __('Join the Challenge');?>" class="btn long yellow cta-bracket-challenge">
            <h4><?php echo __('Join  the Challenge'); ?></h4></a><?php } ?><?php } ?>
    </div>

    <div class="round-container">
        <ul class="round-nav">
            <li class="round-link current" data-round="round-1"><?php echo __('Round 1');?></li>
            <li class="round-link" data-round="round-2"><?php echo __('Round 2');?></li>
            <li class="round-link" data-round="round-3"><?php echo __('Round 3');?></li>
            <li class="round-link" data-round="round-4"><?php echo __('Winner');?></li>
        </ul>
    </div>
    <div class="round-bracket-container">
        <div class="rounds">
            <div id="round-1" class="round round-content current">
                <?php  
                $i = 0;
                foreach ($bracketRound1 as $fight) { $i++;
                    $status = get_field('fight_status', $fight);

                    if (get_field('fight_winner', $fight)) {
                        $winner = (get_field('fight_winner', $fight));
                        $winners[2][$i] = $winner[0];
                    }
                    $fighters = get_field('fight_fighters', $fight); 
                    $status = get_field('fight_status', $fight);
                    ?>
                    <div class="fight <?php if ($status == 'finished') echo 'selected'; ?>">

                        <div class="bracket-card <?php if ($winner[0] == $fighters[0] -> ID) echo 'selected'; ?>" data-next="#r2-<?php echo $i; ?>" data-fighter="<?php echo $fighters[0] -> ID; ?>">
                            <div class="left">
                                <div class="image">
                                    <?php echo get_the_post_thumbnail( $fighters[0] -> ID, 'medium' ); ?>
                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php $country = wp_get_post_terms($fighters[0] -> ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="Check">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <p><?php echo $fighters[0] -> post_title; ?></p>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->

                        <div class="against">
                            <h4>VS</h4>
                        </div> <!-- .against -->

                        <div class="bracket-card <?php if ($winner[0] == $fighters[1] -> ID) echo 'selected'; ?>" data-next="#r2-<?php echo $i; ?>"" data-fighter="<?php echo $fighters[1] -> ID; ?>">
                            <div class="left">
                                <div class="image">
                                    <?php echo get_the_post_thumbnail( $fighters[1] -> ID, 'medium' ); ?>
                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php $country = wp_get_post_terms($fighters[1] -> ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="Check">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <p><?php echo $fighters[1] -> post_title; ?></p>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->
                    </div> <!-- .fight -->

                    <?php unset($winner); ?>

                    <?php } ?>
                </div> <!-- .round -->

                <div id="round-2" class="round round-content">
                    <?php $fighters = get_field('fight_fighters', $bracketRound2[0]); ?>
                    <?php $status = get_field('fight_status', $bracketRound2[0]); ?>
                    <?php
                    if (get_field('fight_winner', $bracketRound2[0])) {
                        $winner = (get_field('fight_winner', $bracketRound2[0]));
                        $winners[3][1] = $winner[0];
                    } 
                    $status = get_field('fight_status', $bracketRound2[0]);
                    ?>
                    <div class="fight <?php if ($status == 'finished') echo 'selected'; ?>">
                        <div class="bracket-card <?php if ($winner[0] == $fighters[0] -> ID) echo 'selected'; ?>" id="r2-1" data-next="#r3-1">
                            <div class="left">
                                <div class="image">
                                    <?php if ($fighters[0]) { ?><?php echo get_the_post_thumbnail( $fighters[0] -> ID, 'medium' ); ?><?php } else { ?><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="Placeholder image"><?php } ?>
                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php if ($fighters[0]) { ?><?php $country = wp_get_post_terms($fighters[0] -> ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>"><?php } ?>
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="Check">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <?php if ($fighters[0]) { ?><p><?php echo $fighters[0] -> post_title; ?></p><?php } ?>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->

                        <div class="against">
                            <h4>VS</h4>
                        </div> <!-- .against -->

                        <div class="bracket-card <?php if ($winner[0] == $fighters[1] -> ID) echo 'selected'; ?>" id="r2-2" data-next="#r3-1">
                            <div class="left">
                                <div class="image">
                                    <?php if ($fighters[1]) { ?><?php echo get_the_post_thumbnail( $fighters[1] -> ID, 'medium' ); ?><?php } else { ?><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="Placeholder image"><?php } ?>
                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php if ($fighters[1]) { ?><?php $country = wp_get_post_terms($fighters[1] -> ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>"><?php } ?>
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="Check">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <?php if ($fighters[1]) { ?><p><?php echo $fighters[1] -> post_title; ?></p><?php } ?>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->
                    </div> <!-- .fight -->

                    <?php $fighters = get_field('fight_fighters', $bracketRound2[1]); ?>
                    <?php
                    if (get_field('fight_winner', $bracketRound2[1])) {
                        $winner = (get_field('fight_winner', $bracketRound2[1]));
                        $winners[3][2] = $winner[0];
                    } 
                    $status = get_field('fight_status', $bracketRound2[1]);
                    ?>
                    <div class="fight <?php if ($status == 'finished') echo 'selected'; ?>">
                        <div class="bracket-card <?php if ($winner[0] == $fighters[0] -> ID) echo 'selected'; ?>" id="r2-3" data-next="#r3-2">
                            <div class="left">
                                <div class="image">
                                    <?php if ($fighters[0]) { ?><?php echo get_the_post_thumbnail( $fighters[0] -> ID, 'medium' ); ?><?php } else { ?><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="Placeholder image"><?php } ?>
                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php if ($fighters[0]) { ?><?php $country = wp_get_post_terms($fighters[0] -> ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>"><?php } ?>
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="Check">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <?php if ($fighters[0]) { ?><p><?php echo $fighters[0] -> post_title; ?></p><?php } ?>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->

                        <div class="against">
                            <h4>VS</h4>
                        </div> <!-- .against -->

                        <div class="bracket-card <?php if ($winner[0] == $fighters[1] -> ID) echo 'selected'; ?>" id="r2-4" data-next="#r3-2">
                            <div class="left">
                                <div class="image">
                                    <?php if ($fighters[1]) { ?><?php echo get_the_post_thumbnail( $fighters[1] -> ID, 'medium' ); ?><?php } else { ?><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="Placeholder image"><?php } ?>
                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php if ($fighters[1]) { ?><?php $country = wp_get_post_terms($fighters[1] -> ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>"><?php } ?>
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="Check">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <?php if ($fighters[1]) { ?><p><?php echo $fighters[1] -> post_title; ?></p><?php } ?>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->
                    </div> <!-- .fight -->

                </div> <!-- .round -->



                <div id="round-3" class="round round-content">

                    <?php 
                    $fighters = get_field('fight_fighters', $bracketRoundFinal[0]); 
                    if (get_field('fight_winner', $bracketRoundFinal[0])) {
                        $winner = (get_field('fight_winner', $bracketRoundFinal[0]));
                    }
                    $winnerIndex = -1;
                    $status = get_field('fight_status', $bracketRoundFinal[0]);
                    ?>

                    <div class="fight <?php if ($status == 'finished') echo 'selected'; ?>">
                        <div class="bracket-card <?php if ($winner[0] == $fighters[0] -> ID) { echo 'selected'; $winnerIndex = 0; } ?>" id="r3-1" data-next="#rfinal">
                            <div class="left">
                                <div class="image">
                                    <?php if ($fighters[0]) { ?><?php echo get_the_post_thumbnail( $fighters[0] -> ID, 'medium' ); ?><?php } else { ?><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="Placeholder image"><?php } ?>
                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php if ($fighters[0]) { ?><?php $country = wp_get_post_terms($fighters[0] -> ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>"><?php } ?>
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="Check">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <?php if ($fighters[0]) { ?><p><?php echo $fighters[0] -> post_title; ?></p><?php } ?>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->

                        <div class="against">
                            <h4>VS</h4>
                        </div> <!-- .against -->

                        <div class="bracket-card <?php if ($winner[0] == $fighters[1] -> ID)  { echo 'selected'; $winnerIndex = 1; } ?>" id="r3-2" data-next="#rfinal">
                            <div class="left">
                                <div class="image">
                                    <?php if ($fighters[1]) { ?><?php echo get_the_post_thumbnail( $fighters[1] -> ID, 'medium' ); ?><?php } else { ?><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="Placeholder image"><?php } ?>
                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php if ($fighters[1]) { ?><?php $country = wp_get_post_terms($fighters[1] -> ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>"><?php } ?>
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="Check">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <?php if ($fighters[1]) { ?><p><?php echo $fighters[1] -> post_title; ?></p><?php } ?>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->
                    </div> <!-- .fight -->

                </div> <!-- .round -->

                <div id="round-4" class="round round-content">


                    <div class="fight selected">
                        <div class="bracket-card rfinal <?php if ($winner && $status == 'finished') echo 'selected'; ?>">
                            <div class="left">
                                <div class="image">
                                    <?php if ($winnerIndex != -1 && $status == 'finished') { ?><?php echo get_the_post_thumbnail( $fighters[$winnerIndex] -> ID, 'medium' ); ?><?php } else { ?><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="Placeholder image"><?php } ?>
                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php if ($winnerIndex != -1 && $status == 'finished') { ?>
                                        <?php $country = wp_get_post_terms($fighters[$winnerIndex] -> ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>">
                                        <?php } ?>
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="Check">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <?php if ($winnerIndex != -1 && $status == 'finished') { ?><p><?php echo $fighters[$winnerIndex] -> post_title; ?></p><?php } ?>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->
                    </div> <!-- .fight -->


                </div> <!-- .round -->

            </div> <!-- .rounds -->
        </div> <!-- .round-bracket-container -->

    </div> <!-- .sub-content -->


    <?php } ?>
<?php endif; ?>

