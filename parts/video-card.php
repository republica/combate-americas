<li class="card<?php echo ($cardClass) ? ' '.$cardClass : '' ; ?>">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" <?php if ($_GET['access'] == 'st0p') echo 'target="_blank"'; ?>>
        <div class="wrapper">
            <div class="image">
              	<?php the_post_thumbnail(); ?>
            </div>
            <div class="info">
                <button class="btn round yellow border small">
                    <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-play-arrow.svg" alt="Play button">
                </button>
                <div class="title">
                    <p><?php the_title(); ?></p>
                </div>
            </div>
        </div>
    </a>
    <?php if ($cardClass) unset($cardClass); ?>
</li>
