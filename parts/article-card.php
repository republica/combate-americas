<li class="card<?php echo ($cardClass) ? ' '.$cardClass : '' ; ?>">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <div class="wrapper">
            <div class="image">
                <?php the_post_thumbnail(); ?>
            </div>
            <div class="info">
                <div class="title">
                    <p><?php the_title(); ?></p>
                </div>
            </div>
        </div>
    </a>    
    <?php if ($cardClass) unset($cardClass);  ?>
</li>