$(document).ready(function(){

    var main = $('main');
    var width = $(window).width();
    var domain = $('body').data('domain');
    $('body').removeAttr('data-domain');

    // ******************************
    // *
    // *   Header
    // *
    // ******************************

    $(function() {
        var header = $("header");
        // menu btn click action
        $(".menu-btn").click(function(){
            $("body").toggleClass('disable-scroll');
            header.toggleClass('active');
        });
        // trigger menu background-color on scroll
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (main.is('.fighters-page, .fighters-single-page, .video-single-page')) {
                if (scroll >= 0) {
                    $('header').addClass('black');
                }
            } else {
                if (scroll >= 40) {
                    header.addClass('black');
                } else {
                    header.removeClass('black');
                }
            }
        });
        // dropdown menu for desktop
        $("#more").click(function(){
            $('.nav').toggleClass('active');
        });
        
        $("#watch").click(function(){
            $('.watchnav').toggleClass('active')
        })
    
    });
    // ******************************
    // *
    // *   Search 
    // *
    // ******************************

    // open search modal
    $(".search").click(function(){
        $("body").addClass('disable-scroll');
        $('.search-container').addClass('active')
    });
    // close search modal
    $(".search-container .close-btn").click(function(){
        $("body").removeClass('disable-scroll');
        $('.search-container').removeClass('active')
    });

    $(".search-container .overlay").click(function(){
        $("body").removeClass('disable-scroll');
        $('.search-container').removeClass('active')
    });

    // limits number of lines 
    var title = $('.search-container .title')
    var excerpt = $('.search-container .excerpt')
    
    title.each(function(index, element) {
        $clamp(element, { clamp: 2, useNativeClamp: false });
    });

    excerpt.each(function(index, element) {
        $clamp(element, { clamp: 3, useNativeClamp: false });
    });

    // ******************************
    // *
    // *   Home Page & News Page
    // *
    // ******************************

    var screenCheck = function() {
        // console.log(width);
        if (main.hasClass('home-page') || main.hasClass('news-page')) {
            
            var card = $(".card.large");
            var button = $(".card.large button");
            var container = $(".sub-content ul:not(.round-nav)");

            // remove large card on mobile & add slider
            if ((width <= 991) && (card.hasClass('large'))) {
                card.removeClass('active');
                button.addClass('small');
                container.addClass('media-slider');
            } else {
                card.addClass('active');
                button.removeClass('small');
                container.removeClass('media-slider');
            }

            $('.media-slider').bxSlider({
                minSlides: 1,
                maxSlides: 5,
                slideWidth: 220,
                slideMargin: 0,
                infiniteLoop: false,
                nextText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">', 
                prevText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">'             
            });

        }
    }

    $(window).resize(function() {
        width = $(window).width();
        screenCheck(); 
    });

    screenCheck();

    // watch btn on home page hero
    $(".watch-btn .menu").click(function(){
        $('.subnav').toggleClass('active');
    });


    $(".watch-btn .subnav li").click(function(){

        $('.selected-link').attr('href' , $(this).data('url'))
        $('.selected-link .logo').html($(this).find('.logo').html());
        $('.selected-link .text h3').html($(this).find('.channel-name').html());
        $('.subnav').removeClass('active');
        window.open($(this).data('url'), '_blank');

    });

    // ******************************
    // *
    // *   Fighters Hero
    // *
    // ******************************
    
    if (main.hasClass('fighters-page') || main.hasClass('fighter-single-page')) {
        $('header').addClass('black');

        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 500) {
                $('.fighters-page .bx-prev').css('opacity','0');
                $('.fighters-page .bx-next').css('opacity','0');
                $('.fighters-page nav').css('opacity','0');
            } else {
                $('.fighters-page .bx-prev').css('opacity','1');
                $('.fighters-page .bx-next').css('opacity','1');
                $('.fighters-page nav').css('opacity','1');
            }
        });

        $('.fighter-intro-slider').bxSlider({
            touchEnabled: false,
            infiniteLoop: false,
            nextText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">Next', 
            prevText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">Prev'             
        });

    }

    // ******************************
    // *
    // *   Fighter Single Page
    // *
    // ******************************
    
    if (main.hasClass('fighter-single-page')) {
        $('ul.pictures-container li').click(function(){    
            $('.image-modal-container').addClass('active');
        })

        $('.image-modal-container .close-btn').click(function(){    
            $('.image-modal-container').removeClass('active');
        })
    }

    // ******************************
    // *
    // *   Tabs
    // *
    // ******************************
    
    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })

    // ******************************
    // *
    // *   Slider on mobile
    // *
    // ******************************
    
    var mobileCheck = function () {
        if (width <= 768){
            $('.related-vids').addClass('content-slider')
            $('.sidebar').addClass('content-slider-wrapper')
        } else {
            $('.related-vids').removeClass('content-slider')
            $('.sidebar').removeClass('content-slider-wrapper')
        }
    }

    mobileCheck(); 

    $(window).resize(function() {
        width = $(window).width();
        mobileCheck(); 
    });
        
    // ******************************
    // *
    // *  Slider for NOT Home Page
    // *
    // ******************************

    if (!main.hasClass('home-page')) {

        $('.content-slider').bxSlider({
            minSlides: 1,
            maxSlides: 5,
            slideWidth: 220,
            slideMargin: 0,
            infiniteLoop: false,
            nextText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">', 
            prevText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">'             
        });
    }

    // ******************************
    // *
    // *  Tab scroll on click
    // *
    // ******************************

    if (main.hasClass('ranking-page')){

        var tab3 = $('li[data-tab="tab-3"]')
        var tab4 = $('li[data-tab="tab-4"]')
        var tab5 = $('li[data-tab="tab-5"]')
        var tab6 = $('li[data-tab="tab-6"]')

        tab3.click(function() {
            $('.tabs-scroll').animate( { scrollLeft: '+=460' }, 1000);
        });
        tab4.click(function() {
            $('.tabs-scroll').animate( { scrollLeft: '+=460' }, 1000);
        });
        tab5.click(function() {
            $('.tabs-scroll').animate( { scrollLeft: '+=460' }, 1000);
        });
        tab6.click(function() {
            // console.log('click')
            $('.tabs-scroll').animate( { scrollLeft: '+=-460' }, 1000);
        });
    }

    // ******************************
    // *
    // *  Home Page Pre Fight OR Bracket Page
    // *
    // ******************************

    if (main.hasClass('home-page-pre-fight') || main.hasClass('bracket-challenge-page')) {
        
        $('ul.round-nav li').click(function(){
            var round_id = $(this).attr('data-round');
    
            $('ul.round-nav li').removeClass('current');
            // $('.round-content').removeClass('current');
    
            $(this).addClass('current');
            // $("#"+round_id).addClass('current');

            $(this).toggleClass('clicked')

            if ($(this).hasClass('clicked')) {
                $('.round-container').animate( { scrollLeft: '+=95' }, 500);
            } else {
                $('.round-container').animate( { scrollLeft: '-=95' }, 500);
            }
        })

        // scroll to bracket on round click
        $('ul.round-nav li:nth-child(1)').click(function(){
            $('.round-bracket-container').animate( { scrollLeft: '0' }, 500);
            $("html, body").animate({ scrollTop: $('#round-1 .fight:first-of-type .bracket-card:first-of-type').offset().top-150 });
        })

        $('ul.round-nav li:nth-child(2)').click(function(){
            $('.round-bracket-container').animate( { scrollLeft: '338' }, 500);
            $("html, body").animate({ scrollTop: $('#round-2 .fight:first-of-type .bracket-card:first-of-type').offset().top-150 });
        })

        $('ul.round-nav li:nth-child(3)').click(function(){
            $('.round-bracket-container').animate( { scrollLeft: '676' }, 500);
            $("html, body").animate({ scrollTop: $('#round-3 .fight:first-of-type .bracket-card:first-of-type').offset().top-150 });
        })

        $('ul.round-nav li:nth-child(4)').click(function(){
            $('.round-bracket-container').animate( { scrollLeft: '1000' }, 500);
            $("html, body").animate({ scrollTop: $('#round-4 .fight:first-of-type .bracket-card:first-of-type').offset().top-150 });
        })

        // round nav switches to position fixed
        $(document).scroll(function() {
            var nav = $('.round-container');
            var scrollY = $(document).scrollTop();
            var scrollX = $('.round-bracket-container').scrollLeft();

            if (scrollY >= $('.round-bracket-container').offset().top && scrollY <= $('.round-bracket-container').offset().top + $('.round-bracket-container').height()) {
                nav.addClass('fixed');
            } else {
                nav.removeClass('fixed');
            } 
            
        });
    }

    // ******************************
    // *
    // *  Bracket Page
    // *
    // ******************************

    if (main.hasClass('bracket-challenge-page') && main.hasClass('open')) {
        var card                = $('.bracket-card');
        var winners             = '';
        var winnersId           = '';
        var finalBracket        = '';
        var finnalbracket       = '';
        var next                = $(this).parent('round').next()

        card.each(function() {

            $(this).on('click', function() {



                if ($(this).attr('orden-fighter') !== undefined && $(this).attr('id') != 'rfinal' && !$(this).hasClass('selected') && main.hasClass('open')) {

                    $(this).addClass('selected');
                    $(this).parent(".fight").addClass('selected');
                    if ($(this).siblings().hasClass('selected')) {
                        $(this).siblings().removeClass('selected')
                    }
                    if ($('.tooltip') !== undefined) {
                        $('.tooltip').animate({opacity: 0}, 500, function() {
                            $('.tooltip').remove();
                        });
                    }
                    $($(this).data('next')).html($(this).html()).attr('orden-fighter', $(this).attr('orden-fighter')).addClass('copied');

                    if ($($(this).data('next')).attr('id') == 'rfinal') $($(this).data('next')).addClass('selected');

                    var sibling = $(this).siblings('.bracket-card');
                    if ($(sibling[0]).attr('orden-fighter') !== undefined) {
                        var  nextPos  = $($(this).data('next')).data('next');
                        var  tempHTML = $(this).html();
                        while($(nextPos).html() == $(sibling[0]).html()) {
                           $(nextPos).html(tempHTML).attr('orden-fighter', $(this).attr('orden-fighter')).addClass('copied');
                           nextPos = $($(nextPos).data('next'));
                        }
                    }
                    updateBracket();
                    if (winners.length == 8 && finalBracket.length == 15) finalStep();
                }
                if ($(this).attr('id') == 'rfinal' && $(this).hasClass('copied')) {
                    $(this).addClass('selected');
                    $(this).parent(".fight").addClass('selected');
                    updateBracket();
                    if (winners.length == 8 && finalBracket.length == 15) finalStep(); 
                }

            })

        })

        function updateBracket() {
            winners = $('.bracket-card.selected');
            var tempWinners = [];
            for ( var i = 0; i < winners.length; i++ ) {
                tempWinners.push( parseInt($(winners[ i ]).attr('orden-fighter')));
            }
            winners = tempWinners;
            $('.winners input').val(winners);

            var fighters = $('.bracket-card');
            var tempFighters = [];
            for ( var i = 0; i < fighters.length; i++ ) {
                tempFighters.push( parseInt($(fighters[ i ]).attr('orden-fighter')));
            }
            finalBracket = tempFighters;
            $('.fullbracket input').val(finalBracket);
            // console.log(winners);
            // console.log(finalBracket);
            

        }

        function finalStep() {
            var bracketId = new Date().valueOf()
            $('.token input').val(bracketId);
            $('#submit-bracket').addClass('active');
            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) $('body').addClass('fix');
        }


    }

    // close popup modal
    $(".popup-container .close-btn").click(function(){
        $("body").removeClass('disable-scroll');
        $("body").removeClass('fix');
        $('.popup-container').removeClass('active')
    });

    $(".popup-container .overlay").click(function(){
        $("body").removeClass('disable-scroll');
        $('.popup-container').removeClass('active')
    });

    // ******************************
    // *
    // *  Images Modal Slider
    // *
    // ******************************

    $('.image-modal-container .bxslider').bxSlider({
        pagerCustom: '#bx-pager',
        nextText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">', 
        prevText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">'
    });
    
    $(".image-modal-container #bx-pager").bxSlider({
        slideWidth: 100,
        minSlides: 3,
        maxSlides: 7,
        slideMargin: 20,
        controls: true,
        pager: false,
        infiniteLoop: false,
        nextText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">', 
        prevText: '<img src="'+domain+'/wp-content/themes/combate-americas/img/icon-arrow-rounded.svg">'
    });

    $('.add-to-calendar').on('click', function(e) {
        e.preventDefault();

        if ($(this).parent().find('.atcb-list').hasClass('visible')) {
            $(this).parent().find('.atcb-list').css('visibility', 'hidden').removeClass('visible');
        }
        else {
            $(this).parent().find('.atcb-list').css('visibility', 'visible').addClass('visible');
        }
    })


document.addEventListener( 'wpcf7mailsent', function( event ) {
  $('.contact-form').remove();
  $('.contact-form-sent').removeClass('hidden');
  $('.bracket-challenge-page').removeClass('open');
}, false );

    
});