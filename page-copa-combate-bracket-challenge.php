<?php
/*
Template Name: COPA COMBATE - BRACKET CHALLENGE
*/
?>
<?php get_header(); ?>
<?php 
$bracket = get_field('bracket');
// var_dump(ICL_LANGUAGE_CODE);
// var_dump($bracket);
$args = array(
    'post_type'     => 'bracket',
    'post__in'      => array($bracket[0]),
);
query_posts($args); 
if (have_posts()) : 
 	while (have_posts()) : the_post();
		$bracketRound1 = get_field('bracket_round_1');
        $challengeclosed = get_field('close_challenge');
	endwhile;
endif;
wp_reset_query();


if ($_GET['bracket-id']) {
    $formid = $wpdb->get_results("SELECT submit_time FROM ".$wpdb->prefix."cf7dbplugin_submits WHERE (field_name = 'token' AND field_value = '". esc_html($_GET['bracket-id'])."')") ;
    $submit_time = $formid[0] -> submit_time;
    $values = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."cf7dbplugin_submits WHERE (submit_time = '".$submit_time."')") ;
    $form = [];
    foreach ($values as $value) {
       $form[$value->field_name] = $value->field_value;
    }
    $closed = true;

    $winners        = explode(',', $form['winners']);
    $fullbracket    = explode(',', $form['fullbracket']);
}

?>
    <main class="bracket-challenge-page <?php if (!$closed) echo 'open'; ?>">
        <!-- 
        ******************************
        *
        *   Hero
        *
        ******************************
        -->
        <section class="hero">
            <div class="background">
                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/hero-pre-fight.jpg" alt="">
            </div>
            <div class="heading">
                <div class="wrap">
                    <div class="copa-logo">
                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/logo-copa-combate-bracket-challenge.png" alt="">
                    </div>
                </div>
                <div class="subtext">
                    <h1 class="hidden"><?php the_title(); ?></h1>
                    <?php if (!$challengeclosed) { ?>
                        <p><?php echo __('8 fighters. 1 winner. Create the perfect bracket and you could win a trip to the next Combate Americas event in Spain.'); ?></p>
                    <?php } else { ?>
                        <p><?php echo __('The Challenge is closed.'); ?></p>
                    <?php } ?>
                </div>
            </div>
        </section>
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <?php if (!$challengeclosed || $closed == true) { ?>
        <section class="content">
            <div class="sub-content brackets challenge">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo"><?php echo (!$closed) ? __('Build Your Bracket') : __('Bracket Challenge').': <strong>'.$form['full-name'].'</strong>'; ?></h2>
                </div>

                <div class="round-container">
                    <ul class="round-nav">
                        <li class="round-link current" data-round="round-1"><?php echo __('Round 1'); ?></li>
                        <li class="round-link" data-round="round-2"><?php echo __('Round 2'); ?></li>
                        <li class="round-link" data-round="round-3"><?php echo __('Round 3'); ?></li>
                        <li class="round-link" data-round="round-4"><?php echo __('Winner'); ?></li>
                    </ul>
                </div>
                <div class="round-bracket-container">
                    <?php if (!$closed) { ?>
                    <div class="tooltip">
                        <div class="container">
                            <p><?php echo __('Let’s get started. Select the winner of each fight.'); ?></p>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="rounds">
                        <!-- 
                        *****************
                        *    ROUND 1 
                        ***************** 
                        -->
                        <div id="round-1" class="round round-content current">


                <?php  
                $i = 0;
                $initialFighters = []; 
                foreach ($bracketRound1 as $fight) { 
                    $i++;
                    $fighters = get_field('fight_fighters', $fight);                 
                ?>

                    <div class="fight <?php if ($closed) echo "selected"; ?>">

                        <div class="bracket-card <?php if($winners[$i-1] == $fighters[0] -> ID + 3 && $closed) echo "selected"; ?>" data-next="#r2-<?php echo $i; ?>" orden-fighter="<?php echo $fighters[0] -> ID + 3; ?>">
                            <div class="left">
                                <div class="image">
                                    <?php $tumbnail = get_the_post_thumbnail( $fighters[0] -> ID, 'medium' ); ?>
                                    <?php echo $tumbnail; ?>

                                    <?php $initialFighters[$fighters[0] -> ID + 3]['image'] = $tumbnail; ?>
                                    <?php $initialFighters[$fighters[0] -> ID + 3]['id'] = $fighters[0] -> ID + 3; ?>

                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php $country = wp_get_post_terms($fighters[0] -> ID, 'country', array("fields" => "all"));?>
                                        <?php $flag = get_field('country_flag', 'country_'.$country[0]->term_id) ?>
                                        <img src="<?php echo $flag; ?>" alt="<?php echo $country[0]->name;?>">

                                        <?php $initialFighters[$fighters[0] -> ID + 3]['flag'] = $flag; ?>
                                        <?php $initialFighters[$fighters[0] -> ID + 3]['country_name'] = $country[0]->name; ?>
                                    </div>
                                    <div class="vote">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-vote.png" alt="">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <p><?php echo $fighters[0] -> post_title; ?></p>
                                    <?php $initialFighters[$fighters[0] -> ID + 3]['name'] = $fighters[0] -> post_title; ?>
                                </div>
                            </div>
                        </div> <!-- .bracket-card -->

                        <div class="against">
                            <h4>VS</h4>
                        </div> <!-- .against -->

                        <div class="bracket-card <?php if($winners[$i-1] == $fighters[1] -> ID + 3 && $closed) echo "selected"; ?>" data-next="#r2-<?php echo $i; ?>" orden-fighter="<?php echo $fighters[1] -> ID + 3; ?>">
                            
                            <div class="left">
                                <div class="image">
                                    <?php $tumbnail = get_the_post_thumbnail( $fighters[1] -> ID, 'medium' ); ?>
                                    <?php echo $tumbnail; ?>

                                    <?php $initialFighters[$fighters[1] -> ID + 3]['image'] = $tumbnail; ?>
                                    <?php $initialFighters[$fighters[1] -> ID + 3]['id'] = $fighters[1] -> ID + 3; ?>

                                </div>
                                <div class="details">
                                    <div class="flag">
                                        <?php $country = wp_get_post_terms($fighters[1] -> ID, 'country', array("fields" => "all"));?>
                                        <?php $flag = get_field('country_flag', 'country_'.$country[0]->term_id) ?>
                                        <img src="<?php echo $flag; ?>" alt="<?php echo $country[0]->name;?>">

                                        <?php $initialFighters[$fighters[1] -> ID + 3]['flag'] = $flag; ?>
                                        <?php $initialFighters[$fighters[1] -> ID + 3]['country_name'] = $country[0]->name; ?>
                                    </div>
                                    <div class="vote">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-vote.png" alt="">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="info">
                                    <p><?php echo $fighters[1] -> post_title; ?></p>
                                    <?php $initialFighters[$fighters[1] -> ID + 3]['name'] = $fighters[1] -> post_title; ?>
                                </div>
                            </div>




                        </div> <!-- .bracket-card -->
                    </div> <!-- .fight -->


                    <?php } ?>

                        </div> <!-- .round -->

                        <!-- 
                        *****************
                        *    ROUND 2
                        ***************** 
                        -->
                        <div id="round-2" class="round round-content">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight <?php if ($closed) echo "selected"; ?>">
                                <?php $bracketPosition = 8; ?>
                                <?php $winnerPosition = 4; ?>
                                <div class="bracket-card <?php if($winners[$winnerPosition] == $initialFighters[$fullbracket[$bracketPosition]]['id'] && $closed) echo "selected"; ?>" id="r2-1" data-next="#r3-1">
                                    <div class="left">
                                        <div class="image">
                                            <?php if ($closed) { ?>
                                            <?php echo $initialFighters[$fullbracket[$bracketPosition]]['image']; ?>
                                            <?php } else { ?>
                                            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="">
                                            <?php } ?>
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                            <?php if ($closed) { ?>
                                            <img src="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['flag']; ?>" alt="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['country_name']; ?>">
                                            <?php } ?>                                             
                                            </div>                                    
                                            <div class="vote">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-vote.png" alt="">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="">
                                    </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <?php if ($closed) { ?>
                                                <?php echo $initialFighters[$fullbracket[$bracketPosition]]['name']; ?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->
                                <?php $bracketPosition = 9; ?>
                                <div class="bracket-card <?php if($winners[$winnerPosition] == $initialFighters[$fullbracket[$bracketPosition]]['id'] && $closed) echo "selected"; ?>" id="r2-2" data-next="#r3-1">
                                    <div class="left">
                                        <div class="image">
                                            <?php if ($closed) { ?>
                                            <?php echo $initialFighters[$fullbracket[$bracketPosition]]['image']; ?>
                                            <?php } else { ?>
                                            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="">
                                            <?php } ?>
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                            <?php if ($closed) { ?>
                                            <img src="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['flag']; ?>" alt="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['country_name']; ?>">
                                            <?php } ?>                                             
                                            </div>                                    
                                            <div class="vote">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-vote.png" alt="">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="">
                                    </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <?php if ($closed) { ?>
                                                <?php echo $initialFighters[$fullbracket[$bracketPosition]]['name']; ?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                            <!-- 
                            *****************
                            *    FIGHT 2 
                            ***************** 
                            -->
                            <div class="fight <?php if ($closed) echo "selected"; ?>">
                                <?php $bracketPosition = 10; ?>
                                <?php $winnerPosition = 5; ?>
                                <div class="bracket-card <?php if($winners[$winnerPosition] == $initialFighters[$fullbracket[$bracketPosition]]['id'] && $closed) echo "selected"; ?>" id="r2-3" data-next="#r3-2">
                                    <div class="left">
                                        <div class="image">
                                            <?php if ($closed) { ?>
                                            <?php echo $initialFighters[$fullbracket[$bracketPosition]]['image']; ?>
                                            <?php } else { ?>
                                            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="">
                                            <?php } ?>
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                            <?php if ($closed) { ?>
                                            <img src="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['flag']; ?>" alt="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['country_name']; ?>">
                                            <?php } ?>                                             
                                            </div>                                    
                                            <div class="vote">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-vote.png" alt="">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="">
                                    </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <?php if ($closed) { ?>
                                                <?php echo $initialFighters[$fullbracket[$bracketPosition]]['name']; ?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->
                                <?php $bracketPosition = 11; ?>
                                <div class="bracket-card <?php if($winners[$winnerPosition] == $initialFighters[$fullbracket[$bracketPosition]]['id'] && $closed) echo "selected"; ?>" id="r2-4" data-next="#r3-2">
                                    <div class="left">
                                        <div class="image">
                                            <?php if ($closed) { ?>
                                            <?php echo $initialFighters[$fullbracket[$bracketPosition]]['image']; ?>
                                            <?php } else { ?>
                                            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="">
                                            <?php } ?>
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                            <?php if ($closed) { ?>
                                            <img src="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['flag']; ?>" alt="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['country_name']; ?>">
                                            <?php } ?>                                             
                                            </div>                                    
                                            <div class="vote">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-vote.png" alt="">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="">
                                    </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <?php if ($closed) { ?>
                                                <?php echo $initialFighters[$fullbracket[$bracketPosition]]['name']; ?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->

                        </div> <!-- .round -->

                        <!-- 
                        *****************
                        *    ROUND 3
                        ***************** 
                        -->
                        <div id="round-3" class="round round-content">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->                            
                            <div class="fight <?php if ($closed) echo "selected"; ?>">
                                <?php $bracketPosition = 12; ?>
                                <?php $winnerPosition = 6; ?>
                                <div class="bracket-card <?php if($winners[$winnerPosition] == $initialFighters[$fullbracket[$bracketPosition]]['id'] && $closed) echo "selected"; ?>" id="r3-1" data-next="#rfinal">
                                    <div class="left">
                                        <div class="image">
                                            <?php if ($closed) { ?>
                                            <?php echo $initialFighters[$fullbracket[$bracketPosition]]['image']; ?>
                                            <?php } else { ?>
                                            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="">
                                            <?php } ?>
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                            <?php if ($closed) { ?>
                                            <img src="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['flag']; ?>" alt="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['country_name']; ?>">
                                            <?php } ?>                                             
                                            </div>                                    
                                            <div class="vote">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-vote.png" alt="">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="">
                                    </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <?php if ($closed) { ?>
                                                <?php echo $initialFighters[$fullbracket[$bracketPosition]]['name']; ?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->

                                <div class="against">
                                    <h4>VS</h4>
                                </div> <!-- .against -->
                                <?php $bracketPosition = 13; ?>
                                <div class="bracket-card <?php if($winners[$winnerPosition] == $initialFighters[$fullbracket[$bracketPosition]]['id'] && $closed) echo "selected"; ?>" id="r3-2" data-next="#rfinal">
                                    <div class="left">
                                        <div class="image">
                                            <?php if ($closed) { ?>
                                            <?php echo $initialFighters[$fullbracket[$bracketPosition]]['image']; ?>
                                            <?php } else { ?>
                                            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="">
                                            <?php } ?>
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                            <?php if ($closed) { ?>
                                            <img src="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['flag']; ?>" alt="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['country_name']; ?>">
                                            <?php } ?>                                             
                                            </div>                                    
                                            <div class="vote">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-vote.png" alt="">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="">
                                    </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <?php if ($closed) { ?>
                                                <?php echo $initialFighters[$fullbracket[$bracketPosition]]['name']; ?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->
                            
                        </div> <!-- .round -->

                        <!-- 
                        *****************
                        *    ROUND 4
                        ***************** 
                        -->
                        <div id="round-4" class="round round-content">
                            <!-- 
                            *****************
                            *    FIGHT 1 
                            ***************** 
                            -->
                            <div class="fight selected">
                                <?php $bracketPosition = 14; ?>
                                <?php $winnerPosition = 7; ?>
                                <div class="bracket-card <?php if($winners[$winnerPosition] == $initialFighters[$fullbracket[$bracketPosition]]['id'] && $closed) echo "selected"; ?>" id="rfinal">
                                    <div class="left">
                                        <div class="image">
                                            <?php if ($closed) { ?>
                                            <?php echo $initialFighters[$fullbracket[$bracketPosition]]['image']; ?>
                                            <?php } else { ?>
                                            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/profile-img.png" alt="">
                                            <?php } ?>
                                        </div>
                                        <div class="details">
                                            <div class="flag">
                                            <?php if ($closed) { ?>
                                            <img src="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['flag']; ?>" alt="<?php echo $initialFighters[$fullbracket[$bracketPosition]]['country_name']; ?>">
                                            <?php } ?>                                             
                                            </div>                                    
                                            <div class="vote">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-vote.png" alt="">
                                    </div>
                                    <div class="pick">
                                        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-check.png" alt="">
                                    </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="info">
                                            <?php if ($closed) { ?>
                                                <?php echo $initialFighters[$fullbracket[$bracketPosition]]['name']; ?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div> <!-- .bracket-card -->
                            </div> <!-- .fight -->
                            
                        </div> <!-- .round -->

                    </div> <!-- .rounds -->
                </div> <!-- .round-bracket-container -->
                
            </div> <!-- .sub-content -->

        </section>
        <?php } ?>
    </main>


 <?php if (!$challengeclosed) { ?>
<div class="popup-container" id="submit-bracket">
    <div class="overlay"></div>
    <div class="main-content">
        
        <div class="close-btn">
            <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-close.svg" alt="">
        </div>

        <div class="container contact-form">
            <h2><?php echo __('Congratulations!'); ?></h2>
            <p><?php echo __('You’ve completed your bracket. Now, enter your name and email below for a chance to win a trip to our next fight in Spain.'); ?></p>
            <?php echo do_shortcode(get_field('contact_form_shortcode')); ?>
        </div>

        <div class="container hidden contact-form-sent">
            <h2><?php echo __('Thank you for entering the Copa Combate Bracket Challenge'); ?></h2>
            <p><?php echo __('Tune in on November 11th to see how your bracket holds up and watch the Quest for the Unbeatable Nation unfold.'); ?></p>
 <!--            <h3><?php echo __('Share the Challenge!'); ?></h3>
            <ul class="challenge-share-links">
                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" title="Share to Facebook"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/fb-blue.svg" alt="Logo Facebook"></a></li>
                <li><a href="https://twitter.com/home?status=<?php echo get_permalink(); ?>" title="Share to Twitter"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/tw-blue.svg" alt="Logo Twitter"></a></li>
            </ul> -->
        </div>
    </div>
</div>
<?php } ?>
<?php get_footer(); ?>