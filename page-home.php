<?php
/*
Template Name: HOME
*/
?>
<?php 
	get_header();

	$template = 'templates/'.get_field('home_template');
	get_template_part($template);
	?>
		<div style="display: none;">Updated!</div>
	<?php
	get_footer(); 
?>