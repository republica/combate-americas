<?php get_header(); ?>

    <main class="contact-page">
        <section class="content">
            <div class="main-heading">
                <div class="background">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="wrapper">
                    <h1 class="mach"><?php the_title(); ?></h1>
                </div>
            </div>
            <div class="sub-content">
                <div class="wrapper">
                    <?php the_content(); ?>
                </div> <!-- .wrapper -->
            </div>
        </section>
    </main>
<?php get_footer(); ?>