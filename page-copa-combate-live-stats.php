<?php
/*
Template Name: COPA COMBATE LIVE STATS
*/
get_header(); ?>
    <main class="bracket-challenge-page live-stats">
        <!-- 
        ******************************
        *
        *   Hero
        *
        ******************************
        -->
        <section class="hero">
            <div class="background">
                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/hero-pre-fight.jpg" alt="">
            </div>
            <div class="heading">
                <div class="subtext">
                    <h1 class="mach"><?php the_title(); ?></h1>
                </div>
            </div>
        </section>
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content bgwhite">

            <?php the_content(); ?>

        </section>
    </main>
<?php get_footer(); ?>
