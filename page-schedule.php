<?php
/*
Template Name: SCHEDULE
*/
?>
<?php get_header(); ?>

<main class="schedule-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
    -->
    <script type="text/javascript">(function () {
        if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
        if (window.ifaddtocalendar == undefined) { window.ifaddtocalendar = 1;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
            var h = d[g]('body')[0];h.appendChild(s); }})();
        </script>
        <style type="text/css" media="screen">
            .atcb-link {
                opacity: 0 !important;
                height: 0 !important;
                display: none !important;
                padding: 0 !important;
            }
            .atc-style-blue .atcb-item, .atc-style-blue .atcb-item-link {
                padding:0 !important;
            }
            .atc-style-blue .atcb-item a, .atc-style-blue .atcb-item-link a { 
                padding: 3px !important;
            }
            .atc-style-blue .atcb-item a:hover, .atc-style-blue .atcb-item-link a:hover { 
                color: #999999;
            }
            .atcb-list {
                margin-top: -10px !important;
            }
        </style>
         <link href="https://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css">
        <section class="content">
            <div class="main-heading">
                <div class="background">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="wrapper">
                    <h1 class="mach"><?php echo __('Upcoming Events'); ?></h1>
                </div>
            </div>
            <div class="sub-content">
                <div class="wrapper">
                    <div class="title-wrapper">
                        <h2 class="mach"><?php echo __('Schedule'); ?></h2>
                    </div>
                    <ul class="tabs">
                        <li class="tab-link current" data-tab="tab-1"><?php echo __('Upcoming Events'); ?></li>
                        <li class="tab-link" data-tab="tab-2">Past Events</li>
                    </ul>

                    <div id="tab-1" class="tab-content current">
 
                        <div class="date-wrapper">
                            <h2 class="robo" style="text-transform: uppercase;"><?php echo __('December'); ?> 2017</h2>
                        </div>

                        <ul class="upcoming-events">
                            <li>
                                <div class="schedule-card white">
                                    <div class="date" style="text-align: center;">
                                        <div class="month"><?php echo __('Dec'); ?></div>
                                        <div class="day mach">1</div>
                                        <div class="time"></div>
                                    </div>
                                    <div class="info">
                                        <div class="title">Combate 19</div>
                                        <div class="location">San Antonio, TX</div>
                                        <div class="venue">
                                            <div class="pin">
                                                <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-pin.svg" alt="">
                                            </div>
                                            <div class="address">Freeman Coliseum.<br>3201 E Houston St, San Antonio, TX 78219</div>
                                        </div>
                                        <div class="cal">
                                            <a href="#" class="add-to-calendar">
                                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-add.svg" alt="">
                                                <p><?php echo __('Add to Calendar'); ?></p>
                                            </a>
                                            <span class="addtocalendar atc-style-blue">
                                                <var class="atc_event">
                                                    <var class="atc_date_start">2017-12-01 23:30:00</var>
                                                    <var class="atc_date_end">2017-12-01 23:30:00</var>
                                                    <var class="atc_timezone">America/New_York</var>
                                                    <var class="atc_title">Combate 19</var>
                                                    <var class="atc_description">Combate 19 - Combate Americas - https://www.combateamericas.com/</var>
                                                    <var class="atc_location">Freeman Coliseum.<br>3201 E Houston St, San Antonio, TX 78219</var>
                                                    <var class="atc_organizer">Combate Americas</var>
                                                    <var class="atc_organizer_email">info@combateamericas.com</var>
                                                </var>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>

                        <div class="date-wrapper">
                            <h2 class="robo" style="text-transform: uppercase;"><?php echo __('January'); ?> 2018</h2>
                        </div>

                        <ul class="upcoming-events">

                            <li>
                                <div class="schedule-card white">
                                    <div class="date" style="text-align: center;">
                                        <div class="month"><?php echo __('Jan'); ?></div>
                                        <div class="day mach">26</div>
                                        <div class="time"></div>
                                    </div>
                                    <div class="info">
                                        <div class="title">Combate Estrellas</div>
                                        <div class="location">Los Angeles</div>
                                        <div class="venue">
                                            <div class="pin">
                                                <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-pin.svg" alt="">
                                            </div>
                                            <div class="address">Venue TBA<br></div>
                                        </div>
                                        <div class="cal">
                                            <a href="#" class="add-to-calendar">
                                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-add.svg" alt="">
                                                <p><?php echo __('Add to Calendar'); ?></p>
                                            </a>
                                            <span class="addtocalendar atc-style-blue">
                                                <var class="atc_event">
                                                    <var class="atc_date_start">2018-01-26 23:30:00</var>
                                                    <var class="atc_date_start">2018-01-26 23:30:00</var>
                                                    <var class="atc_timezone">America/New_York</var>
                                                    <var class="atc_title">Combate 20</var>
                                                    <var class="atc_description">Combate Estrellas - Combate Americas - https://www.combateamericas.com/</var>
                                                    <var class="atc_location">Los Angeles, Venue TBA</var>
                                                    <var class="atc_organizer">Combate Americas</var>
                                                    <var class="atc_organizer_email">info@combateamericas.com</var>
                                                </var>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                        <!-- <button class="btn long yellow border">More Events</button> -->

                    </div> <!-- #tab-1 -->

                    <div id="tab-2" class="tab-content">

                        <div class="date-wrapper">
                            <h2 class="robo"><?php echo __('Past Events'); ?></h2>
                        </div>

                        <ul class="past-events">
                                <div class="schedule-card blue">
                                    <div class="date" style="text-align: center;">
                                        <div class="month">Nov</div>
                                        <div class="day mach">11</div>
                                        <div class="time">11:30PM / 10:30 C</div>
                                    </div>
                                    <div class="info">
                                        <div class="title">Copa Combate</div>
                                        <div class="location">Cancún, México</div>
                                        <div class="venue">
                                            <div class="pin">
                                                <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-pin.svg" alt="">
                                            </div>
                                            <div class="address">Grand Oasis Cancún.<br>Km. 16.5, Boulevard Kukulcan Lt45-47, Zona Hotelera, 77500 Cancún, QROO, Mexico</div>
                                        </div>
                                    </div>
                                </div>
                        </ul>
                    </div>



                </div> <!-- .wrapper -->
            </div>
        </section>
    </main>

    <?php get_footer(); ?>