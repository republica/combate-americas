

<div id="demo">



<?php 
   // var_dump(ip_info("Visitor", "Country"));

    $channel = array(
        'name'  => 'Telemundo',
        'logo'  => 'logo-telemundo.png',
        'url'   => 'http://www.telemundodeportes.com/',
    );
    $channels["tele"] = $channel;

    $channel = array(
        'name'  => 'NBC SN',
        'logo'  => 'logo-nbcsn.png',
        'url'   => 'http://www.nbcsports.com/'
    );  
    $channels["nbcsn"] = $channel;

    $channel = array(
        'name'  => 'Azteca 7',
        'logo'  => 'logo-azteca7.png',
        'url'   => 'http://www.aztecadeportes.com/combate-americas/'
    );  
    $channels["azte7"] = $channel;

    $channel = array(
        'name'  => 'ESPN LATAM',
        'logo'  => 'logo-espn-latam.png',
        'url'   => 'http://www.espn.com/watch/'
    );  
    $channels["espn-latam"] = $channel;

    $channel = array(
        'name'  => 'Gol Television',
        'logo'  => 'logo-goltelevision.png',
        'url'   => 'http://goltelevision.com/'
    );  
    $channels["gol-television"] = $channel;


    $lang       = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $country    = ip_info("Visitor", "Country");
    $area       = ip_info("Visitor", "continent_code");

    // $defaultChannel= ($lang = 'en') ? "nbcsn" : "tele";
    $defaultChannel = "tele";
    if ($area == 'SA') {
        $defaultChannel = ($country == "Mexico") ? "azte7" : "espn-latam";        
    }
    $defaultChannel = ($country == 'Spain' || $country == 'España') ? 'gol-television' : $defaultChannel;

?>
</div>
<div class="watch-btn">
    <div class="info">
        <a class="selected-link" href="<?php echo $channels[$defaultChannel]['url'] ?>" target="_blank" title="<?php echo $channels[$defaultChannel]['name'] ?>">
            <div class="logo">
                <img class="tele-logo" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/channels/<?php echo $channels[$defaultChannel]['logo'] ?>" alt="<?php echo $channels[$defaultChannel]['name'] ?>">
            </div>
            <div class="text">
                <h4><?php echo __('Watch live on'); ?></h4>
                <h3><?php echo $channels[$defaultChannel]['name'] ?></h3>
            </div>
        </a>
    </div>
    <div class="menu">
        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-watch-menu.svg" alt="">
    </div>
    <ul class="subnav">
        <?php foreach ($channels as $channel) { ?>
            <li data-url="<?php echo $channel['url'] ?>">
                <div class="logo">
                    <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/channels/<?php echo $channel['logo'] ?>" alt="<?php echo $channel['name'] ?>">
                </div>
                <div class="channel-name"><?php echo $channel['name'] ?></div>
                <div class="channel-num">&nbsp;</div>
                <div class="play">
                    <a class="btn round small border yellow" href="<?php echo $channel['url'] ?>" title="<?php echo $channel['name'] ?>" target="_blank">
                        <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-play-arrow.svg" alt="Icon">
                    </a>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>