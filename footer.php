	
    <section class="subscribe">
         <div class="wrapper">
             <h3 class="robo"><?php echo __('Subscribe to our Newsletter'); ?></h3>

<!-- Begin MailChimp Signup Form -->
<!-- <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css"> -->
<style type="text/css">
    /*#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }*/
    /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
       We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<form action="https://combateamericas.us15.list-manage.com/subscribe/post?u=e9e5432a5a48fc8bb4bb3e0db&amp;id=0885f9a0ee" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
<div class="input-wrapper">
    <input type="email" value="" name="EMAIL" class="required" id="mce-EMAIL" placeholder="<?php echo __('Enter your email address'); ?>">
    <input type="submit" value="<?php echo __('Subscribe'); ?>" name="subscribe" id="mc-embedded-subscribe" class="button btn long yellow ">
</div>
    <div id="mce-responses" class="clear">
        <div class="response" id="mce-error-response" style="display:none"></div>
        <div class="response" id="mce-success-response" style="display:none"></div>
    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e9e5432a5a48fc8bb4bb3e0db_0885f9a0ee" tabindex="-1" value=""></div>
    </div>
</form>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='phone';fnames[4]='MMERGE4';ftypes[4]='zip';fnames[5]='MMERGE5';ftypes[5]='number';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->


         </div>
    </section>

    <footer>
        <div class="wrapper">
        <?php wp_nav_menu( array(
          'theme_location' => 'menu-footer',
          'menu_id' => 'menu-header',
          'menu_class' => 'nav',
          'container' => 'ul',
          )); ?>

            <div class="social">
                <div class="title">
                    <h3 class="robo desktop"><?php echo __('Follow Us'); ?></h3>
                </div>
                <ul class="links">
                    <li><a href="https://www.facebook.com/combateamericasofficial" title="Facebook" target="_blank"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-fb.svg" alt=""></a></li>
                    <li><a href="https://twitter.com/combateamericas" title="Twitter" target="_blank"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-tw.svg" alt=""></a></li>
                    <li><a href="http://instagram.com/combateamericas" title="Instagram" target="_blank"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-ig.svg" alt=""></a></li>
                    <li><a href="https://www.youtube.com/channel/UClvfFzGFsKp5x4V8SaeG9Dw" title="Youtube" target="_blank"><img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-yt.svg" alt=""></a></li>
                </ul>
            </div>
        </div>
    </footer>



    <!--  SEARCH MODAL -->

    <div class="search-container">
        <div class="overlay"></div>
        <div class="main-content">
            <div class="search-section">
                <div class="close-btn">
                    <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-close.svg" alt="">
                </div>
                <h3 class="title"><?php echo __('Search Combate Americas'); ?></h3>
                <div class="input-wrapper">
                    <input type="text" placeholder="Search">
                    <button class="btn round yellow">
                        <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-search.svg" alt="">
                    </button>
                </div>
                <p class="result-num">43 Results Found</p>
            </div>
            <div class="results">
                <ul class="result-container">
                    <li>
                        <a href="">
                            <div class="image">
                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                            </div>
                            <div class="info">
                                <h4 class="title">Jose Estrada "Froggy"</h4>
                                <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <div class="image">
                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">
                                <button class="btn round yellow border">
                                    <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-play-arrow.svg" alt="">
                                </button>
                            </div>
                            <div class="info">
                                <h4 class="title">Jose Estrada "Froggy"</h4>
                                <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <div class="image">
                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                            </div>
                            <div class="info">
                                <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                                <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <div class="image">
                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                            </div>
                            <div class="info">
                                <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                                <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <div class="image">
                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                            </div>
                            <div class="info">
                                <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                                <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <div class="image">
                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                            </div>
                            <div class="info">
                                <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                                <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <div class="image">
                                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/temp/img-ref<?php echo rand(1, 7); ?>.jpg" alt="">

                            </div>
                            <div class="info">
                                <h4 class="title">THE MMA TAKEOVER: Q&A WITH COMBATE’S JOSE “FROGGY”</h4>
                                <p class="excerpt">Jose “Froggy” Estrada is the unquestioned face of Combate Americas.  But with that lofty role comes plenty of pressure.  Lorem ipsum dolor sit amet</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</body>
</html>

	<?php wp_footer(); ?>

    <script src="<?php echo TEMPLATE_DIRECTORY; ?>/app.js?v=1.5"></script>
    <script src="<?php echo TEMPLATE_DIRECTORY; ?>/js/scripts.js?v=1.5"></script>
    
</body>
</html>