<?php
/*
Template Name: FIGHTERS
*/
?>
<?php get_header(); ?>

<main class="fighters-page">
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
    -->
    <style type="text/css" media="screen">
    .main-heading ul,
    .main-heading li,
    .main-heading div {
        background-color: transparent !important;
    }
</style>
<section class="content">
    <div class="main-heading">
        <ul class="fighter-intro-container fighter-intro-slider">
            <?php $featuredFighters = get_field('fp_featured_fighters'); ?>
            <?php
            $args = array(
                'post_type'     => 'fighter',
                'post__in'      => $featuredFighters,
                'posts_per_page' => -1,
                'orderby'   => 'meta_value_num',
                'meta_key'  => 'fighter_ranking',
                'order'  => 'ASC',
            );
            query_posts($args); 
            $excludeFighter = array();
            $i = 0;
            if (have_posts()) :
                while (have_posts()) : the_post(); $i++; if ($i == 1) { $excludeFighter[] = $post->ID; } ?>
                <li>
                    <div class="fighter-intro">
                        <div class="wrapper">
                            <div class="background">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="info">
                                <div class="info-top">
                                    <div class="name">
                                        <h4 class="robo"><?php the_field('fighter_first_name'); ?></h4>
                                        <h1 class="mach"><?php the_field('fighter_last_name'); ?></h1>
                                        <span>"<?php the_field('fighter_alias'); ?>"</span>
                                    </div>
                                    <div class="weight-class">
                                        <p><?php $weightClass = get_the_terms( $post->ID, 'weight-class', '', ' / ' ); ?></p>

                                        <p><?php echo $weightClass[0] -> name;  ?></p>
                                        <?php $country = wp_get_post_terms($post->ID, 'country', array("fields" => "all"));?>
                                        <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>">
                                    </div>
                                </div>
                                <div class="info-bottom">
                                    <p><?php echo limitword(get_the_excerpt(), 30); ?></p>
                                    <a href="<?php the_permalink(); ?>" class="btn long yellow" title="<?php the_title(); ?>"><?php echo __('View Profile'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .fighter-intro -->
                </li>
            <?php endwhile;
            endif; wp_reset_query(); ?>
        </ul>
    </div> <!-- .heading --> 
    <div class="sub-content">
        <?php 
        $terms = get_terms( array(
            'taxonomy' => 'weight-class',
            'hide_empty' => true,
        ) );
                // var_dump($terms);
        foreach ($terms as $term) { ?>
        <div class="title">
            <h1 class="mach"><?php echo $term->name; ?></h1>
        </div>
        <?php
        $args = array(
            'post_type'     => 'fighter',
            'posts_per_page' => -1,
            'orderby'       => 'menu_order',
            'order'         => 'ASC',
            'tax_query' => array(
                'relation' => 'AND', 
                array(
                    'taxonomy' => 'weight-class',
                    'field' => 'slug',
                    'terms' => array($term->slug),
                ),
            )
        );

        query_posts($args); 
        if (have_posts()) : ?>
        <ul class="fighters-container">
            <?php while (have_posts()) : the_post();  ?>
                <li class="fighter-card">
                    <a href=" <?php the_permalink(); ?> " title="<?php the_title(); ?>">
                        <div class="image">
                            <?php the_post_thumbnail('large'); ?>
                        </div>
                        <div class="info">
                            <div class="flag">
                                <?php $country = wp_get_post_terms($post->ID, 'country', array("fields" => "all"));?>
                                <img src="<?php the_field('country_flag', 'country_'.$country[0]->term_id) ?>" alt="<?php echo $country[0]->name;?>">
                            </div>
                            <h3 class="robo"><?php the_field('fighter_first_name'); ?></h3>
                            <h3 class="mach"><?php the_field('fighter_last_name'); ?></h3>
                            <span class="alias">"<?php the_field('fighter_alias'); ?>"</span>
                        </div>
                    </a>
                </li>
            <?php endwhile; ?>
        </ul>
    <?php endif; wp_reset_query(); ?>
    <?php } ?>
</div>
</section>
</main>

<?php get_footer(); ?>