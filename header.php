<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php get_template_part('meta'); ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M7TZRNQ');</script>
<!-- End Google Tag Manager -->

<!-- Reddit Conversion Pixel -->
<script>
var i=new Image();i.src="https://alb.reddit.com/snoo.gif?q=CAAHAAABAAoACQAAAAicUsQqAA==&s=BD7_TeXSbhiPHTi88jhTywoYln2ho7ByIjHx6QhG0jI=";
</script>
<noscript>
<img height="1" width="1" style="display:none"
src="https://alb.reddit.com/snoo.gif?q=CAAHAAABAAoACQAAAAicUsQqAA==&s=BD7_TeXSbhiPHTi88jhTywoYln2ho7ByIjHx6QhG0jI="/>
</noscript>
<!-- DO NOT MODIFY -->
<!-- End Reddit Conversion Pixel -->
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '642432489478121'); 
        fbq('track', 'PageView');
    </script>
    <noscript>
       <img height="1" width="1" 
       src="https://www.facebook.com/tr?id=642432489478121&ev=PageView
       &noscript=1"/>
   </noscript>
   <!-- End Facebook Pixel Code -->
</head>

<body class="dev" data-domain="<?php echo get_site_url(); ?>">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7TZRNQ"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <script>
        // INSTRUCTIONS
        // The VersaTag code should be placed at the top of the <BODY> section of the HTML page.
        // To ensure that the full page loads as a prerequisite for the VersaTag
        // being activated (and the working mode is set to synchronous mode), place the tag at the bottom of the page. Note, however, that this may
        // skew the data for slow-loading pages, and in general is not recommended.
        // If the VersaTag code is configured to run in async mode, place the tag at the bottom of the page before the end of the <BODY > section.

        //
        // NOTE: You can test if the tags are working correctly before the campaign launches
        // as follows:  Browse to http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=at, which is 
        // a page that lets you set your local machine to 'testing' mode.  In this mode, when
        // visiting a page that includes a VersaTag, a new window will open, showing you
        // the tags activated by the VersaTag and the data sent by the VersaTag tag to the Sizmek servers.
        //
        // END of instructions (These instruction lines can be deleted from the actual HTML)

        var versaTag = {};
        versaTag.id = "8035";
        versaTag.sync = 0;
        versaTag.dispType = "js";
        versaTag.ptcl = "HTTP";
        versaTag.bsUrl = "bs.serving-sys.com/BurstingPipe";
        //VersaTag activity parameters include all conversion parameters including custom parameters and Predefined parameters. Syntax: "ParamName1":"ParamValue1", "ParamName2":"ParamValue2". ParamValue can be empty.
        versaTag.activityParams = {
        //Predefined parameters:
        "Session":""
        //Custom parameters:
    };
        //Static retargeting tags parameters. Syntax: "TagID1":"ParamValue1", "TagID2":"ParamValue2". ParamValue can be empty.
        versaTag.retargetParams = {};
        //Dynamic retargeting tags parameters. Syntax: "TagID1":"ParamValue1", "TagID2":"ParamValue2". ParamValue can be empty.
        versaTag.dynamicRetargetParams = {};
        // Third party tags conditional parameters and mapping rule parameters. Syntax: "CondParam1":"ParamValue1", "CondParam2":"ParamValue2". ParamValue can be empty.
        versaTag.conditionalParams = {};
    </script>
    <script id="ebOneTagUrlId" src="http://ds.serving-sys.com/SemiCachedScripts/ebOneTag.js"></script>
    <noscript>
        <iframe src="http://bs.serving-sys.com/BurstingPipe?
        cn=ot&amp;
        onetagid=8035&amp;
        ns=1&amp;
        activityValues=$$Session=[Session]$$&amp;
        retargetingValues=$$$$&amp;
        dynamicRetargetingValues=$$$$&amp;
        acp=$$$$&amp;"
        style="display:none;width:0px;height:0px"></iframe>
    </noscript>
    <!-- 
    ******************************
    *
    *   Header
    *
    ******************************
-->
<header>
    <div class="logo">
        <h1>
            <a href="html-home.php">
                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/logo.svg" alt="Combate Americas">
            </a>
        </h1>
    </div>
    <div class="menu">
        <div class="menu-btn">
            <span></span>
        </div>
        <ul class="nav">
            <li><a href="<?php echo SITE_URL; ?>/<?php echo __('fighters'); ?>/"><?php  echo __('Fighters'); ?></a></li>
            <li id="watch">
                <a href="javascript:void(0);"><?php echo __('Watch'); ?></a>


<?php 
   // var_dump(ip_info("Visitor", "Country"));

    $channel = array(
        'name'  => 'Telemundo',
        'logo'  => 'logo-telemundo.png',
        'url'   => 'http://www.telemundodeportes.com/',
    );
    $channels["tele"] = $channel;

    $channel = array(
        'name'  => 'NBC SN',
        'logo'  => 'logo-nbcsn.png',
        'url'   => 'http://www.nbcsports.com/'
    );  
    $channels["nbcsn"] = $channel;

    $channel = array(
        'name'  => 'Azteca 7',
        'logo'  => 'logo-azteca7.png',
        'url'   => 'http://www.aztecadeportes.com/combate-americas/'
    );  
    $channels["azte7"] = $channel;

    $channel = array(
        'name'  => 'ESPN LATAM',
        'logo'  => 'logo-espn-latam.png',
        'url'   => 'http://www.espn.com/watch/'
    );  
    $channels["espn-latam"] = $channel;

    $channel = array(
        'name'  => 'Gol',
        'logo'  => 'logo-goltelevision.png',
        'url'   => 'http://goltelevision.com/'
    );  
    $channels["gol-television"] = $channel;


    $channel = array(
        'name'  => 'Internet TV',
        'logo'  => 'logo-itv.png',
        'url'   => 'https://www.internetv.tv/'
    );  
    $channels["itv"] = $channel;
?>
            <ul class="watchnav">
                <?php foreach ($channels as $channel) { ?>
                    <li data-url="<?php echo $channel['url'] ?>">
                        <div class="logo">
                            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/channels/<?php echo $channel['logo'] ?>" alt="<?php echo $channel['name'] ?>">
                        </div>
                        <div class="channel-name"><?php echo $channel['name'] ?></div>
                        <div class="channel-num">&nbsp;</div>
                        <div class="play">
                            <a class="btn round small border yellow" href="<?php echo $channel['url'] ?>" title="<?php echo $channel['name'] ?>" target="_blank">
                                <img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-play-arrow.svg" alt="Icon">
                            </a>
                        </div>
                    </li>
                <?php } ?>
            </ul>

            </li>
            <li><a href="<?php echo SITE_URL; ?>/<?php echo __('schedule'); ?>/"><?php  echo __('Schedule'); ?></a></li>
            <li><a href="<?php echo SITE_URL; ?>/<?php echo __('videos-feed'); ?>/"><?php  echo __('Videos'); ?></a></li>
            <li><a href="<?php echo SITE_URL; ?>/<?php echo __('news'); ?>/"><?php  echo __('News'); ?></a></li>
            <li id="more" class="desktop">
                <a href="javascript:void(0);"><?php  echo __('More'); ?></a>
                <ul class="subnav">
                    <!-- <li><a href="<?php echo SITE_URL; ?>/<?php echo __('rankings'); ?>/"><?php  echo __('Rankings'); ?></a></li> -->
                    <li><a href="<?php echo SITE_URL; ?>/<?php echo __('about'); ?>/"><?php  echo __('About'); ?></a></li>
                    <li><a href="<?php echo SITE_URL; ?>/<?php echo __('contact'); ?>/"><?php  echo __('Contact'); ?></a></li>
                </ul>
            </li>
            <!-- <li class="mobile"><a href="<?php echo SITE_URL; ?>/<?php echo __('ranking'); ?>/"><?php  echo __('Ranking'); ?></a></li> -->
            <li class="mobile"><a href="<?php echo SITE_URL; ?>/<?php echo __('about'); ?>/"><?php  echo __('About'); ?></a></li>
            <li class="mobile"><a href="<?php echo SITE_URL; ?>/<?php echo __('contact'); ?>/"><?php  echo __('Contact'); ?></a></li>
        </ul>
    </div>
    <div class="options">
        <div class="language">
            <?php languages_list(); ?>
        </div>
<!--         <div class="search">
            <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-search.svg" alt="">
        </div> -->
        <div class="tickets">
            <a href="https://www1.ticketmaster.com/ts1-promotions-presents-combate-americas-combate-san-antonio-texas-12-01-2017/event/3A005357CF973C98" class="btn long yellow" title="<?php echo __('Tickets') ?>" target="_blank"><?php echo __('Tickets') ?></a>
        </div>
    </div>
</header>

