// Basic setup, grunt watch for sass compile, min style.css and concat/uglify js


module.exports = function(grunt) {
	grunt.initConfig({
		sass: {
			dev: {
				files: {
					'style.css' : 'scss/style.scss',
				}
			},
			build: {
				files: {
					'style.css' : 'scss/style.scss',
				}
			}
		},
		concat: {
			js: {
				src: [
					'js/jquery-3.2.1.min.js',
					'js/svg.js',
					'js/jquery.bxslider.min.js',
					'js/clamp.min.js',
					'js/jquery.modal.min.js'
				],
				dest: 'app.js'
			},
			css: {
				src: [
					'css/jquery.bxslider.min.css',
					'css/jquery.modal.min.css'
				],
				dest: 'app.css'
			},
		},
		uglify: {
			app: {
				files: {
					'app.js': ['app.js']
				}
			}
		},
		cssmin: {
			app: {
				files: {
					'style.css': ['style.css']
				}
			}
		},
		imagemin: {
			core: {
				expand: true,
				src: ['img/*.{png,jpg,gif,jpeg}']
			}
		},
		watch: {
			sass: {
				files: ['scss/*.scss', 'scss/**/*.scss'],
				tasks: ['sass:dev']
			},
			scripts: {
				files: ['js/scripts.js'],
				tasks: ['concat:js','uglify']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');	
	grunt.registerTask('build', ['sass:dev', 'concat', 'cssmin', 'uglify']);
	grunt.registerTask('default', ['build']);
};
