<main class="home-page-pre-fight home-page">
    <!-- 
    ******************************
    *
    *   Hero
    *
    ******************************
-->
<section class="hero">
    <div class="background">

        <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/hero-pre-fight.jpg" alt="">

    </div>
    <div class="heading">
        <div class="wrap">
            <div class="copa-logo">
                <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/logo-copa-combate.png" alt="">
            </div>
        </div>
        <div class="wrap">
            <h3 class="mach"><?php echo __('Saturday'); ?> Nov 11</h3>
            <h3 class="robo">11:30PM / 10:30 C</h3>
            <div class="watch-live">
                <?php include( locate_template( 'parts/watch-btn.php', false, false ) ); ?>
            </div>
        </div>
        <div class="wrap">
            <h3>Cancun, Mexico</h3>
        </div>
        <div class="wrap">
            <a href="<?php echo SITE_URL; ?>/videos-feed/copa-combate-nov-11-2017/" class="btn yellow long">
                <?php echo __('About Copa Combate'); ?>
            </a>
        </div>
    </div>
</section>
    <!-- 
    ******************************
    *
    *   Content
    *
    ******************************
-->
<section class="content">

    <?php 
    $bracket = get_field('home_bracket');

    if ($bracket) { 
        include( locate_template( 'parts/home-bracket.php', false, false ) );
        // include( locate_template( 'parts/bracket-copa-combate-challenge.php', false, false ) );
     } ?>


    <?php
    $trendings = get_field( 'home_trendings_news' );
    $args = array(
        'post_type'     => 'video',
        'post_not__in'  => $trendings,
        'posts_per_page' => 7,
        'orderby'       => 'date',
        'order'         => 'DESC',
    );
    query_posts($args); 
    $i = 0;
    if (have_posts()) : ?>
    <div class="sub-content videos">
        <!-- section title -->
        <div class="title white">
            <h2 class="robo"><?php echo __('Videos'); ?></h2>
            <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
        </div>
        <!-- video container -->
        <ul class="video-container">
            <?php while (have_posts()) : the_post();  $i++; if ($i == 1) $cardClass = 'large'; 
            include( locate_template( 'parts/video-card.php', false, false ) );
            endwhile; ?>
        </ul> <!-- .video-container -->
    </div> <!-- .sub-content -->
<?php endif; wp_reset_query(); ?>


<?php
$trendings = get_field( 'home_trendings_news' );
$args = array(
    'post_type'     => 'post',
    'post_not__in'  => $trendings,
    'posts_per_page' => 7,
    'orderby'       => 'date',
    'order'         => 'DESC',
);
query_posts($args); 
$i = 0; 
if (have_posts()) :
    ?>
    <div class="sub-content news">
        <!-- section title -->
        <div class="title white">
            <h2 class="robo"><?php echo __('Latest News'); ?></h2>
            <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
        </div>
        <!-- news container -->
        <ul class="news-container">
            <?php
            while (have_posts()) : the_post();  $i++; if ($i == 1) $cardClass = 'large'; 
                include( locate_template( 'parts/article-card.php', false, false ) );
                endwhile; ?>
        </ul> <!-- .news-container -->
    </div> <!-- .sub-content -->
<?php endif; wp_reset_query(); ?>
</section>
</main>