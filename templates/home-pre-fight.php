    <main class="home-page">
        <!-- 
        ******************************
        *
        *   Hero
        *
        ******************************
        -->

        <section class="hero hero-alt combate-estrellas">
            <div class="background">
                <?php the_post_thumbnail(); ?>
            </div>
            <div class="hero-info">
                <div class="title-text">
                    <img src="<?php echo TEMPLATE_DIRECTORY; ?>/img/hero-alt/title-text-combate-estrellas.jpg" alt="Queen Warriors">
                </div>
<!--                 <div class="logos">
                    <img class="ticketmaster" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/hero-alt/logo-ticketmaster.png" alt="Ticketmaster logo">
                    <img class="freeman" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/hero-alt/logo-freeman.png" alt="Freeman logo">
                    <img class="telemundo" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/hero-alt/logo-telemundo.png" alt="Telemundo logo">
                </div> -->
            </div>
        </section>


        </section>


        
        <!-- 
        ******************************
        *
        *   Content
        *
        ******************************
        -->
        <section class="content">

            <div class="sub-content trending">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Trending Now</h2>
                    <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
                </div>
                <!-- video container -->
                <ul class="trending-container">
                    <?php
                        $trendings = get_field( 'home_trendings_news' );
                        $args = array(
                            'post_type'     => array('post', 'video'),
                            'post__in'      => $trendings,
                            'posts_per_page' => 5,
                            'orderby'       => 'date',
                            'order'         => 'DESC',
                        );
                        query_posts($args); 
                        if (have_posts()) :
                            while (have_posts()) : the_post();  
                                $card = ($post->post_type == 'post') ? 'article-card' : 'video-card' ;
                                include( locate_template( 'parts/'.$card.'.php', false, false ) );
                        endwhile;
                    endif; wp_reset_query(); ?>
                </ul> <!-- .trending-container -->
            </div> <!-- .sub-content -->

            <div class="sub-content videos">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Videos</h2>
                    <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
                </div>
                <!-- video container -->
                <ul class="video-container">
                    <?php
                        $trendings = get_field( 'home_trendings_news' );
                        $args = array(
                            'post_type'     => 'video',
                            'post_not__in'  => $trendings,
                            'posts_per_page' => 7,
                            'orderby'       => 'date',
                            'order'         => 'DESC',
                        );
                        query_posts($args); 
                        $i = 0;
                        if (have_posts()) :
                            while (have_posts()) : the_post();  $i++; if ($i == 1) $cardClass = 'large'; 
                            include( locate_template( 'parts/video-card.php', false, false ) );
                        endwhile;
                    endif; wp_reset_query(); ?>
                </ul> <!-- .video-container -->
            </div> <!-- .sub-content -->

            <div class="sub-content news">
                <!-- section title -->
                <div class="title white">
                    <h2 class="robo">Latest News</h2>
                    <span><img class="svg" src="<?php echo TEMPLATE_DIRECTORY; ?>/img/icon-arrow.svg" alt=""></span>
                </div>
                <!-- news container -->
                <ul class="news-container">
                    <?php
                        $trendings = get_field( 'home_trendings_news' );
                        $args = array(
                            'post_type'     => 'post',
                            'post_not__in'  => $trendings,
                            'posts_per_page' => 7,
                            'orderby'       => 'date',
                            'order'         => 'DESC',
                        );
                        query_posts($args); 
                        $i = 0;
                        if (have_posts()) :
                            while (have_posts()) : the_post();  $i++; if ($i == 1) $cardClass = 'large'; 
                            include( locate_template( 'parts/article-card.php', false, false ) );
                        endwhile;
                    endif; wp_reset_query(); ?>
                </ul> <!-- .news-container -->
            </div> <!-- .sub-content -->

        </section>
    </main>
