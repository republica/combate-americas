<?php
error_reporting(E_ALL);

/* Var definitions
----------------------------------------------------------------------------------------------------*/
define('TEMPLATE_DIRECTORY', ''.get_template_directory_uri().'');
define('SITE_URL', ''.get_bloginfo('url').'');

/* Default timezone
----------------------------------------------------------------------------------------------------*/
date_default_timezone_set('America/New_York');

/* Theme language 
----------------------------------------------------------------------------------------------------*/
setlocale(LC_TIME,"");
setlocale(LC_TIME,"en_US");

/* WordPress Remove Head
----------------------------------------------------------------------------------------------------*/
remove_action('wp_head', 'wp_generator');

/* Images resolution.
----------------------------------------------------------------------------------------------------*/
add_filter( 'jpeg_quality', 'jpeg_full_quality' );
function jpeg_full_quality( $quality ) { return 100; }

add_theme_support('post-thumbnails'); 

/* Custom logo for the Login page
----------------------------------------------------------------------------------------------------*/
add_action("login_head", "my_login_head");

function my_login_head() {
    echo "
    <style>
        body.login #login h1 a {
            background: url('".TEMPLATE_DIRECTORY."/img/logo.svg') no-repeat scroll center center black;
            background-size: 300px auto;
            height: 100px;
            margin: 0 0 20px;
            padding: 0;
            width: 100%;
            border-radius: 10px;
        }
    </style>
    ";
}

/* Custom logo for the Login page
----------------------------------------------------------------------------------------------------*/
add_action("admin_head", "fix_box_zindex");

function fix_box_zindex() {
    echo "
    <style>
        .postbox {
            z-index: 0;
        }
    </style>
    ";
}

/* Custom login header URL.
----------------------------------------------------------------------------------------------------*/
add_action( 'login_headerurl', 'my_custom_login_url' );
function my_custom_login_url() {
    return ''.get_bloginfo('url').'';
}

/* Custom Login Logo Alt text
----------------------------------------------------------------------------------------------------*/
add_action("login_headertitle","my_custom_login_title");

function my_custom_login_title()
{
    return 'Combate Americas';
}

/* Custom footer text in the admin
----------------------------------------------------------------------------------------------------*/
function remove_footer_admin () {
    echo "Repúplica";
} 

add_filter('admin_footer_text', 'remove_footer_admin'); 


/* Register Navigation Menus
----------------------------------------------------------------------------------------------------*/
register_nav_menus( array(
    'menu-header' => 'Menu Header',
    'menu-footer' => 'Menu Footer',
    ));


add_theme_support( 'post-thumbnails' );


/* Limit string by words number
----------------------------------------------------------------------------------------------------*/
function limitword( $str, $num ) {
    $palabras = preg_split( '/[\s]+/', $str, -1, PREG_SPLIT_OFFSET_CAPTURE );
    if( isset($palabras[$num][1]) ){
        $str = trim(substr( $str, 0, $palabras[$num][1] )).'...';
    }
    unset( $palabras, $num );
    return trim( $str );
} 

/* Limit string by characters number.
----------------------------------------------------------------------------------------------------*/
function truncate ($str, $length, $trailing='...')    {
    $length-=strlen($trailing);
    if (strlen($str) > $length) 
    {
        $res = substr($str,0,$length);
        $res .= $trailing;
    } 
    else 
    { 
        $res = $str.$trailing; 
    }
    unset($str, $trailing, $length);
    return trim($res);
}

function the_truncate ($str, $length, $trailing='...')  {
    echo truncate($str, $length, $trailing);
}

/*Fix Amnesia for WP problems in Category pagination.
----------------------------------------------------------------------------------------------------*/
function cure_wp_amnesia_on_query_string($query_string){
    if ( !is_admin() ){
        if ( isset( $query_string['category_name'] ) ) {
            $query_string['posts_per_page'] = get_option( 'posts_per_page' );
        }
    }
    return $query_string;
}

add_filter('request', 'cure_wp_amnesia_on_query_string');

/* Add Post, Page & Custom Post Type to Feed
----------------------------------------------------------------------------------------------------*/
function myfeed_request($qv) {
    if (isset($qv['feed']) && !isset($qv['post_type']))
        $qv['post_type'] = array('post', 'producto');
    return $qv;
}
add_filter('request', 'myfeed_request');

/* Add excerpt field to pages
----------------------------------------------------------------------------------------------------*/
add_action('init', 'my_custom_init');
function my_custom_init() {
    add_post_type_support( 'page', 'excerpt' );
}

/* Redirect Only Post Result.
----------------------------------------------------------------------------------------------------*/
function redirect_to_post(){
    global $wp_query;
    if( (is_tax() || is_category()) && $wp_query->post_count == 1 ){
        the_post();
        $post_url = get_permalink();
        wp_redirect( $post_url );
    }
}
add_action('template_redirect', 'redirect_to_post'); 


/* Get Post Permalink and Date by ID.
----------------------------------------------------------------------------------------------------*/
function get_post_permalink_and_date_by_id($id, $posttype = 'post') {
    $data = array();
    if ($id > 0)  {
        $args = array(
            'p' => $id,
            'post_type' => array(
                $posttype
                ),
            );
        query_posts($args);
        if (have_posts()) :
            while (have_posts()) : the_post();
        $data['permalink'] = get_the_permalink();
        $data['date'] = get_the_date();
        endwhile; 
        endif;
        wp_reset_query();
    }
    return $data;
}

/* Get the post thumbnail by post ID
----------------------------------------------------------------------------------------------------*/
function get_thumbnail_by_id($id, $posttype = 'post') {
    $thumbnail = '';
    if ($id > 0)  {
        $args = array(
            'p' => $id,
            'post_type' => array(
                $posttype
                ),
            );
        query_posts($args);
        if (have_posts()) :
            while (have_posts()) : the_post();
        $post_thumbnail_detail = get_post(get_post_thumbnail_id());
        $thumbnail = $post_thumbnail_detail;
        endwhile; 
        endif;
    }
    wp_reset_query();
    return $thumbnail;
}


// define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
// define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
// define('ICL_DONT_LOAD_LANGUAGES_JS', true);

function languages_list(){
    $languages = icl_get_languages('skip_missing=0&orderby=code&order=desc');
    if(!empty($languages)){
        foreach($languages as $l){
            if($l['active']) {
            } else {
                $access = ($_GET['access'] == 'st0p') ? '?access=st0p' : '' ;
                echo '<p class="icl-'.$l['language_code'].'">';
                echo '<a title="'.$l['native_name'].'" class="lang_sel_other" href="'.$l['url'].$access.'">'.$l['native_name'].'</a>';  
                echo '</p>';
            }
        }
    }
}



/* Definition of Custom Post Types and Custom Taxonomies
----------------------------------------------------------------------------------------------------*/
get_template_part('inc/define-cpt');

/* Definition of Option's General options. Social links, footer links.
----------------------------------------------------------------------------------------------------*/
get_template_part('inc/theme-options');


/* Add "btn and underline" classes to main menu links. Front end hack. 
----------------------------------------------------------------------------------------------------*/
function add_menuclass($ulclass) {
   return (strpos($ulclass, 'id="menu-header"') >= 0) ? preg_replace('/<a /', '<a class="btn underline"', $ulclass) : $ulclass;
}
// add_filter('wp_nav_menu','add_menuclass');


/* Widgets definition 
----------------------------------------------------------------------------------------------------*/
// function rep_init_widget() {
//     register_sidebar( array(
//         'name'          => __( 'News Sidebar', 'republica' ),
//         'id'            => 'republica-news-sidebar',
//         'description'   => '',
//         'before_widget' => '<aside>',
//         'after_widget'  => '</aside>',
//         ) );

// }
// add_action( 'widgets_init', 'rep_init_widget' );



/* Changes the default posts labels to news.
----------------------------------------------------------------------------------------------------*/  
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );


function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}


function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

add_filter('max_srcset_image_width', create_function('', 'return 1;'));




/**
 * @param $formName string
 * @param $fieldName string
 * @param $fieldValue string
 * @return bool
 */
function is_already_submitted($formName, $fieldName, $fieldValue) {
    require_once(ABSPATH . 'wp-content/plugins/contact-form-7-to-database-extension/CFDBFormIterator.php');
    $exp = new CFDBFormIterator();
    $atts = array();
    $atts['show'] = $fieldName;
    $atts['filter'] = "$fieldName=$fieldValue";
    $atts['unbuffered'] = 'true';
    $exp->export($formName, $atts);
    $found = false;
    while ($row = $exp->nextRow()) {
        $found = true;
    }
    return $found;
}
 
/**
 * @param $result WPCF7_Validation
 * @param $tag array
 * @return WPCF7_Validation
 */
function my_validate_email($result, $tag) {
    $formNameEn = 'Copa_Combate_Bracket_Challenge_en'; // Change to name of the form containing this field
    $formNameEs = 'Copa_Combate_Bracket_Challenge_es'; // Change to name of the form containing this field
    $fieldName = 'email'; // Change to your form's unique field name
    $errorMessageEn = 'Email has already been submitted'; // Change to your error message
    $errorMessageEs = 'Su email ya se encuentra en nuestra base de datos'; // Change to your error message
    $name = $tag['name'];

    if ($name == $fieldName) {
        if (is_already_submitted($formNameEn, $fieldName, $_POST[$name])) {
            $result->invalidate($tag, $errorMessageEn);
        }
        if (is_already_submitted($formNameEs, $fieldName, $_POST[$name])) {
            $result->invalidate($tag, $errorMessageEs);
        }
    }

    return $result;
}
 
// use the next line if your field is a **required email** field on your form
add_filter('wpcf7_validate_email*', 'my_validate_email', 10, 2);
// use the next line if your field is an **email** field not required on your form
add_filter('wpcf7_validate_email', 'my_validate_email', 10, 2);
 
// use the next line if your field is a **required text** field
add_filter('wpcf7_validate_text*', 'my_validate_email', 10, 2);
// use the next line if your field is a **text** field field not required on your form 
add_filter('wpcf7_validate_text', 'my_validate_email', 10, 2)


?>